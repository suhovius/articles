Suggested Metatags: ffmpeg, frei0r, ffmpegthumbnailer, video effects, sound effects, video processing, sidekiq, carrierwave_backgrounder, carrierwave, streamio-ffmpeg, ruby, rails, mongoid, mongodb, nosql, ffmpeg cpu usage limit

**Video Processing at Ruby on Rails with FFmpeg and Frei0r effects**
=============

Introduction
-------------

In this article I am going describe possibilities of standard gems for video processing at Ruby on Rails, plus some custom tweaks and hacks for fancy video effects.

For database in this project I am going to use NoSQL database [MongoDB](https://www.mongodb.com/) as I plan to embed metadata inside Video record since I do not need these datas to be separated from Video record. For me it looks as more natural way of storing json / hash data structures in databases. It is also possible with more traditional SQL databases like PostgreSQL or MySQL since they have native support for json fields now. Or this json data could be just serialized into string and saved in some long text field.

Videos are going to be processed in background with [sidekiq](https://github.com/mperham/sidekiq) gem.

All video processing is going to happen with [FFmpeg](https://ffmpeg.org/) utility: some simple video effects and audio effects plus fancy video effects from [Frei0r](https://frei0r.dyne.org/) plugin.

For real time processing progress refresh I am going to use web socket nofications with rails' [Action Cable](http://edgeguides.rubyonrails.org/action_cable_overview.html). It should be performant enough for this small demo application.


TL;DR
-------------
If you do not want to read all this article than project repository is here:
[TODO: Add link to Yalantis github. This project should be hosted there](http://add_link_to_yalantis_github_host_this_project_there)

Project Setup
-------------

**1. Install FFmpeg with all options**

If your operating system is OSX than you could use homebrew for installation

```
brew install ffmpeg --with-fdk-aac --with-frei0r --with-libvo-aacenc --with-libvorbis --with-libvpx --with-opencore-amr --with-openjpeg --with-opus --with-schroedinger --with-theora --with-tools
```

Maybe not all of these options are needed for current project but I included almost all possible to avoid issues when something is not working if some extension has not been installed.

On other Linux OS like Ubuntu or Debian it might require to install ffmpeg from sources. Maybe this guide could be helpful [https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu](https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu)

**2. Fix frei0r**

When I installed ffmpeg with Frei0r, it's effects were not working. To check if you have such issue run ffmpeg with frei0r effects:

`ffmpeg -v debug -i 1.mp4 -vf frei0r=glow:0.5 output.mpg`

you might see something like this:

```
[Parsed_frei0r_0 @ 0x7fe7834196a0] Looking for frei0r effect in '/Users/user/.frei0r-1/lib/glow.dylib' [Parsed_frei0r_0 @ 0x7fe7834196a0] Looking for frei0r effect in '/usr/local/lib/frei0r-1/glow.dylib' [Parsed_frei0r_0 @ 0x7fe7834196a0] Looking for frei0r effect in '/usr/lib/frei0r-1/glow.dylib'
```

FFmpeg is probably right. If you will ls -l /usr/local/lib/frei0r-1/ you will see that plug-ins are installed with .so extension.

On my machine (OSX 10.12.5, ffmpeg 3.3.2, frei0r-1.6.1), I just did something like this:

`for file in /usr/local/lib/frei0r-1/*.so ; do cp $file "${file%.*}.dylib" ; done`

Also I had to set this environment variable with path to folder where these .dylib files are stored:

`export FREI0R_PATH=/usr/local/Cellar/frei0r/1.6.1/lib/frei0r-1`

This solution looks like some strange hack, but finally after all this operations I have got Frei0r working well.

**2. Install ffmpeg thumbnailer**

`brew install ffmpegthumbnailer` this utility would be used for video thumbnail creation.

**3. Install imagemagick**

`brew install imagemagick` also project would generate many thumbnails and these thumbnails would require to be processed and stored.

**4. Install MongoDB, Redis**

`brew install mongodb` project's database

`brew install redis` storage for sidekiq job parameters and data

**5. Ruby and Rails**

I assume that you already have rvm (ruby version manager), ruby and rails installed so I am not going to cover this here.

JFYI: I have been using `ruby 2.3.0` and `Rails 5.0.1` for this project.

Development
-----------

**1. Install and configure gems**

First create new rails project without default active record, since we are going to use mongoDB for data storage.

`rails new video_manipulator --skip-active-record`

Than add all needed gems to Gemfile

```
# Video Processing
gem 'streamio-ffmpeg'
gem 'carrierwave-video', github: 'evgeniy-trebin/carrierwave-video'
gem 'carrierwave-video-thumbnailer',
    github: '23shortstop/carrierwave-video-thumbnailer'
gem 'carrierwave_backgrounder', github: '23shortstop/carrierwave_backgrounder'
gem 'kaminari-mongoid', '~> 0.1.0'

# Data Storage (Order matters here. It should be required after carrierwave-video)
gem 'mongoid', '~> 6.1.0'
gem 'carrierwave-mongoid', require: 'carrierwave/mongoid'

# Background jobs
gem 'sidekiq'

# Web site styles
gem 'materialize-sass'

# This would be used for JSON serialization
gem 'active_model_serializers', '~> 0.10.0'
```

As you can see some of these gems are installed directly from github without rubygems. In general it is not always good to install gems like this, but these ones have some fixes and some more contemporary changes, since originals of these gems at rubygems have been updated a few years ago last time.

Install these gems with `bundle install` command.

Configure these gems inside project. Read about these gems configuration here:

* [https://github.com/23shortstop/carrierwave_backgrounder](https://github.com/23shortstop/carrierwave_backgrounder)
* [https://docs.mongodb.com/mongoid/master/tutorials/mongoid-installation/](https://docs.mongodb.com/mongoid/master/tutorials/mongoid-installation/)
* [https://github.com/mkhairi/materialize-sass](https://github.com/mkhairi/materialize-sass) this gem is used for styles. You might use some other styles bootstrap gem. Or create your own html markup styling.

Note that due compatibility with `carrierwave_backgrounder` we are using sidekiq workers, not modern active job workers from rails.

So, inside `config/initializers/carrierwave_backgrounder.rb`
sidekiq is configured as backend for this video processing and uploading queues

```
CarrierWave::Backgrounder.configure do |c|
  c.backend :sidekiq, queue: :carrierwave
end

```

For `mongoid` gem we just generate it's generic config.

`rails g mongoid:config`

It is enough for this demo research app. Of course for production/commercial apps you should not store database config file at project's repository since it might contain some database connection credentials. The same relies for mongoDB since by default installation it doesn't have any password for database connection, so in production you have to set up password for mongoDB too.

**2. Generate basic scaffold for Videos**

We are not going to add authorization, users, etc. Since it is just demo application for videos processing with Rails.

So, let’s generate scaffold for videos with this command:

`rails generate scaffold Video title:string file_tmp:string file_processing:boolean file_duration:integer progress:float needs_thumbnails:boolean`

**3. Create and configure other data models**

Inside Video add field definitions:

```
class Video
  include Mongoid::Document
  include Mongoid::Timestamps

  # ActiveRecord has this by default.
  # Since it is Mongoid::Document
  # we might need this for data processing at background jobs
  # https://github.com/rails/activemodel-globalid
  # I allows model to be serialized / deserialized by unique identifier
  include GlobalID::Identification

  embeds_many :thumbnails

  embeds_many :processing_metadatas

  # Basic fields configuration
  field :title, type: String
  # file_tmp is used for temporary file saving while
  # it processed and stored in the background by carrierwave_backgrounder gem
  field :file_tmp, type: String
  # file_processing attribute is managed by carrierwave_backgrounder when
  # to track is file under processing or not
  field :file_processing, type: Boolean
  # Video duration would be extracted from video metadata
  field :file_duration, type: Integer
  # This would be updated during file processing to track processing progress
  # from 0 to 1
  field :progress, type: Float, default: 0
  # Config option: generate thumbnails for each video second or not
  field :needs_thumbnails, type: Boolean, default: false

  # All user applied effects is stored as array
  field :effects, type: Array, default: []

  # File ffmpeg metadata is stored at hash
  field :metadata, type: Hash, default: {}

  # mount_on is specified here because without it gem
  # would name filename attribute as file_filename
  # In some way it looks logical. But in our case it is strage to have
  # file_filename attribute at database
  mount_uploader :file, ::VideoUploader, mount_on: :file
  process_in_background :file
  store_in_background :file, ::VideoSaverWorker
  validates_presence_of :file

  # The same as above. We do not want to have watermark_image_filename attribute
  mount_uploader :watermark_image, ::ImageUploader, mount_on: :watermark_image

  validates :title, presence: true
  validate :effects_allowed_check

  # This is not good idea to let this thing update database field many times
  # It is made just to test how it works
  def processing_progress(format, format_options, new_progress)
    ProgressCalculator.new(self, format, format_options, new_progress).update!
  end

  def save_metadata(new_metadata)
    set(
      metadata: new_metadata,
      file_duration: new_metadata[:format][:duration]
    )
  end

  def save_thumbnail_files(files_list)
    files_list.each do |file_path|
      ::File.open(file_path, 'r') do |f|
        thumbnails.create!(file: f)
      end
    end
  end

  # before_transcode callback
  # Callback method accepts format and raw options but we do not need them here
  def processing_init_callback(_, _)
    clean_datas
  end

  def processing_completed_callback
    ::ActionCable.server.broadcast(
      'notifications_channel',
      ::CableData::VideoProcessing::CompletedSerializer.new(self).as_json
    )
  end

  private

  def effects_allowed_check
    effects.each do |effect|
      unless ::VideoUploader::ALLOWED_EFFECTS.include?(effect)
        errors.add(:effects, :not_allowed, effect: effect.humanize)
      end
    end
  end

  def clean_datas
    processing_metadatas.destroy_all
    thumbnails.destroy_all
  end
end
```

`Video` model has two attachments `video` and `watermark_image` CarrierWave file uploaders.


Add `ProcessingMetadata` model. It is embedded inside `Video`.
Each of these records is dedicated to each processing step. And each processing operation has it's own `progress` attribute

```
class ProcessingMetadata
  include Mongoid::Document

  embedded_in :video

  field :step, type: String
  field :format, type: String
  field :progress, type: Float, default: 0
end
```

Add `Thumbnail` model. It is embedded inside `Video`.

```
class Thumbnail
  include Mongoid::Document

  embedded_in :video

  # Here background uploading is not used since this entity would be created in
  # background already
  mount_uploader :file, ::ImageUploader
end
```


Add `ImageUploader`

Run this command `rails generate uploader Image` it would generate attachment uploader for Thumbnail model:

```
class ImageUploader < CarrierWave::Uploader::Base
  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_whitelist
    %w[jpg jpeg gif png]
  end
end
```

Add `VideoUploader`

`rails generate uploader Video`

Here is final version of this video uploader. Probably it is big enough to be refactored into many small classes and modules (one per each effect for example), but for demo application it is more obvious to have everything in one file, so it could be easier to understand what is happening there:

```
class VideoUploader < CarrierWave::Uploader::Base
  # Store and process video in the background
  include ::CarrierWave::Backgrounder::Delay
  # This is custom extension for video metadata extraction
  include ::CarrierWave::Extensions::VideoMetadata
  # Use carrierwave-video gem's methods here
  include ::CarrierWave::Video
  # Use carrierwave-video-thumbnailer gem's methods here
  include ::CarrierWave::Video::Thumbnailer
  # This is custom extension for video thumbnails creation
  include ::CarrierWave::Extensions::VideoMultiThumbnailer

  include ::EncodingConstants

  def extension_whitelist
    %w[mov mp4 3gp mkv webm m4v avi]
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Processing is forced ower original file so when processing is finished,
  # processed file would replace original one. This one line of enforces this:
  process encode: [:mp4, PROCESSED_DEFAULTS]
  # We just do not need original file. And also this would ensure that
  # Thumbnail would be generated from processed file

  def encode(format, opts = {})
    normalization_step(format, opts)
    apply_effect_steps(format, opts)
    apply_watermark_step(format, opts) if model.watermark_image.path.present?
    read_video_metadata_step(format, opts)
    create_thumbnails_step('jpg', opts) if model.needs_thumbnails?
  end

  # Generate one thumbnail from middle (50%) of the file
  # With carrierwave-video-thumbnailer gem
  version :thumb do
    process thumbnail: [
      { format: 'png', quality: 10, size: 200, seek: '50%', logger: Rails.logger }
    ]

    def full_filename(for_file)
      png_name for_file, version_name
    end

    # INFO: This is needed to set proper file content type
    # Solution details
    # https://github.com/evrone/carrierwave-video-thumbnailer/issues/6#issuecomment-28664696
    process :apply_png_content_type
  end

  def png_name(for_file, version_name)
    %(#{version_name}_#{for_file.chomp(File.extname(for_file))}.png)
  end

  def apply_png_content_type(*)
    file.instance_variable_set(:@content_type, 'image/png')
  end

  def audio_effects
    model.effects & AUDIO_EFFECTS.keys.map(&:to_s)
  end

  def video_effects
    model.effects & VIDEO_EFFECTS.keys.map(&:to_s)
  end

  def ordered_effects
    # Audio effects should be applied first
    # since there might be conflict with some video effects
    # (At least with effect that speeds up or slows down video along with audio)
    audio_effects + video_effects
  end

  def normalization_step(format, opts)
    encode_video(
      format,
      opts.merge(
        processing_metadata: { step: 'normalize' },
        callbacks: {
          # Clean previous progress data if encoding happens for existing video record
          # Callback method at model
          before_transcode: :processing_init_callback
        }
      )
    )
  end

  def apply_effect_steps(format, opts)
    ordered_effects.each do |effect|
      encode_video(
        format,
        opts.merge(
          processing_metadata: { step: "apply_#{effect}_effect" }
        )
      ) do |_, params|
        params[:custom] = EFFECT_PARAMS[effect.to_sym]
      end
    end
  end

  def apply_watermark_step(format, opts)
    encode_video(
      format,
      opts.merge(processing_metadata: { step: 'apply_watermark' })
    ) do |_, params|
      params[:watermark] ||= {}
      params[:watermark][:path] = model.watermark_image.path
    end
  end

  def read_video_metadata_step(format, opts)
    read_video_metadata(
      format,
      opts.merge(
        save_metadata_method: :save_metadata,
        processing_metadata: { step: 'read_video_metadata' }
      )
    )
  end

  def create_thumbnails_step(format, _opts)
    create_thumbnails_for_video(
      format,
      progress: :processing_progress,
      save_thumbnail_files_method: :save_thumbnail_files,
      resolution: '300x300',
      vframes: model.file_duration, frame_rate: '1', # create thumb for each second of the video
      processing_metadata: { step: 'create_video_thumbnails' }
    )
  end
end
```

**4. Video processing logic**

The most interesting part of this uploader is encode method. It consists of few steps:

* Video normalization for size, format, video and audio codecs, bitrate, etc
* Processing all user selected effects one by one. Some effects could be applied at one transcoding operation some are not compatible with each other, so due this we process them separately to be sure that everything could be processed properly. Of course it is slower than transcoding all in one operation, but it is more reliable. Also audio effects are applied before video effects. I had some incompatibility errors when audio effects were applied after video effects, so I put them at first place in this effects processing order at `ordered_effects` method
* If `watermark_image` is present than it would be applied also. This functionality is included inside `carrierwave-video` gem.
* Next we read and save video metadata with `read_video_metadata` custom method that is defined here `CarrierWave::Extensions::VideoMetadata`. It is made to be compatible with video processing logic from `carrierwave-video` gem. Also it calls `save_metadata` callback at model to save metadata there.
* Finally if user has requested thumbnails generation than custom processing method `create_thumbnails_for_video` from `CarrierWave::Extensions::VideoMultiThumbnailer` is used. They are saved at model with `save_thumbnail_files` callback method

All encoding constants are placed to `EncodingConstants` module

```
module EncodingConstants
  PROCESSED_DEFAULTS = {
    resolution:           '500x400',
    video_codec:          'libx264',
    constant_rate_factor: '30',
    frame_rate:           '25',
    audio_codec:          'aac',
    audio_bitrate:        '64k',
    audio_sample_rate:    '44100',
    progress: :processing_progress
  }.freeze

  VIDEO_EFFECTS = {
    sepia:
      %w[
        -filter_complex colorchannelmixer=.393:.769:.189:0:.349:.686:.168:0:.272:.534:.131
        -c:a
        copy
      ],
    black_and_white: %w[-vf hue=s=0 -c:a copy],
    vertigo: %w[-vf frei0r=vertigo:0.2 -c:a copy],
    vignette: %w[-vf frei0r=vignette -c:a copy],
    sobel: %w[-vf frei0r=sobel -c:a copy],
    pixelizor: %w[-vf frei0r=pixeliz0r -c:a copy],
    invertor: %w[-vf frei0r=invert0r -c:a copy],
    rgbnoise: %w[-vf frei0r=rgbnoise:0.2 -c:a copy],
    distorter: %w[-vf frei0r=distort0r:0.05|0.0000001 -c:a copy],
    iirblur: %w[-vf frei0r=iirblur -c:a copy],
    nervous: %w[-vf frei0r=nervous -c:a copy],
    glow: %w[-vf frei0r=glow:1 -c:a copy],
    reverse: %w[-vf reverse -af areverse],
    slow_down: %w[-filter:v setpts=2.0*PTS -filter:a atempo=0.5],
    speed_up: %w[-filter:v setpts=0.5*PTS -filter:a atempo=2.0]
  }.freeze

  AUDIO_EFFECTS = {
    echo: %w[-map 0 -c:v copy -af aecho=0.8:0.9:1000|500:0.7|0.5],
    tremolo: %w[-map 0 -c:v copy -af tremolo=f=10.0:d=0.7],
    vibrato: %w[-map 0 -c:v copy -af vibrato=f=7.0:d=0.5],
    chorus: %w[-map 0 -c:v copy -af chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3]
  }.freeze

  EFFECT_PARAMS = VIDEO_EFFECTS.merge(AUDIO_EFFECTS).freeze

  ALLOWED_EFFECTS = EFFECT_PARAMS.keys.map(&:to_s).freeze

  OBLIGATORY_STEPS = %w[normalize read_video_metadata].freeze
end
```

Each processing step receives metadata key `processing_metadata` with step name that would be used for processing progress tracking per each step.

At initial `normalize` step `callbacks` key is specified method that would be called at `Video` instance by `carrierwave-video` gem

After primary file processing is finished than carrierwave would create thumbnail for it with `carrierwave-video-thumbnailer` gem for `:thumb` version.

**5. Custom CarrierWave Extensions**

Custom extension `CarrierWave::Extensions::VideoMetadata` has just one parameter `save_metadata_method` that contains the name of callback at model that is called with extracted metadata. It's code is made to be compatible with `carrierwave-video` gem that's why it has similar logic with it's [encode_video](https://github.com/evgeniy-trebin/carrierwave-video/blob/master/lib/carrierwave/video.rb#L41-L71) method.

Also there is code to provide progress of this operation value. But since it's instant then it sends 1.0 (as 100%) to progress callback method just once.

```
module CarrierWave
  module Extensions
    module VideoMetadata
      def read_video_metadata(format, opts = {})
        # move upload to local cache
        cache_stored_file! unless cached?

        @options = CarrierWave::Video::FfmpegOptions.new(format, opts)

        file = ::FFMPEG::Movie.new(current_path)

        if opts[:save_metadata_method]
          model.send(opts[:save_metadata_method], file.metadata)
        end

        progress = @options.progress(model)

        with_trancoding_callbacks do
          # Here it happens instantly so we provide here value for 100%
          progress&.call(1.0)
        end
      end
    end
  end
end
```

Custom module for `CarrierWave::Extensions::VideoMultiThumbnailer` is also designed to be processed with `carrierwave-video` gem's logic:

```
module CarrierWave
  module Extensions
    module VideoMultiThumbnailer
      def create_thumbnails_for_video(format, opts = {})
        prepare_thumbnailing_parameters_by(format, opts)
        # Create temporary directory where all created thumbnails would be saved
        prepare_tmp_dir

        run_thumbnails_transcoding

        # Run callback for saving thumbnails
        save_thumb_files
        # Remove temporary data
        remove_tmp_dir
      end

      private

      def run_thumbnails_transcoding
        with_trancoding_callbacks do
          if @progress
            @movie.screenshot(*screenshot_options) do |value|
              @progress.call(value)
            end
            # It is ugly hack but this operation returned this in the end
            # 0.8597883597883599 that is not 1.0
            @progress.call(1.0)
          else
            @movie.screenshot(*screenshot_options)
          end
        end
      end

      def prepare_thumbnailing_parameters_by(format, opts)
        # move upload to local cache
        cache_stored_file! unless cached?

        # This `custom: []` is needed to avoid error at
        # CarrierWave::Video::FfmpegOptions#format_params
        @options = CarrierWave::Video::FfmpegOptions.new(
          format, opts.merge(custom: [])
        )

        @movie = ::FFMPEG::Movie.new(current_path)

        if opts[:resolution] == :same
          @options.format_options[:resolution] = @movie.resolution
        end

        yield(@movie, @options.format_options) if block_given?

        @progress = @options.progress(model)
      end

      def base_tmp_dir_path
        "#{::Rails.root}/tmp/videos/#{model.id}"
      end

      def tmp_dir_path
        "#{base_tmp_dir_path}/thumbnails"
      end

      def prepare_tmp_dir
        FileUtils.mkdir_p(tmp_dir_path)
      end

      def remove_tmp_dir
        FileUtils.rm_rf(base_tmp_dir_path) if Dir.exist?(base_tmp_dir_path)
      end

      # Thumbnails are sorted by their creation date
      # to align then in chronological order.
      def thumb_file_paths_list
        Dir["#{tmp_dir_path}/*.#{@options.format}"].sort_by do |filename|
          File.mtime(filename)
        end
      end

      def save_thumb_files
        model.send(@options.raw[:save_thumbnail_files_method], thumb_file_paths_list)
      end

      def screenshot_options
        [
          "#{tmp_dir_path}/%d.#{@options.format}",
          @options.format_params,
          {
            preserve_aspect_ratio: :width,
            validate: false
          }
        ]
      end
    end
  end
end
```

It uses `screenshot` method directly from [streamio-ffmpeg](https://github.com/streamio/streamio-ffmpeg#taking-screenshots) gem. It our case it generates thumbnails for each second of video. Also since this operation is local we have to create, use and remove temporary directories for generated thumbnails with `tmp_dir_path`. Generated images are sent to model's callback that is specified at `save_thumbnail_files_method` parameter.

By some strange reason it's progress value does not always return `1.0` at the end (`0.8597` for example) that is why I had to set manually `1.0` progress value when operation has been finished.


**5. Progress Calculation logic**

Since each transcoding operation could return progress values, I decided to play with it also. Our video processing consists of many steps, each step has it's own progress value, so we need to calculate overall progress that consists of all these steps.

When I was researching this I noticed that progress callback is called very often, near once or twice per second. Since I want to track this progress in database, I decided to update it once per 10% of each step's transcoding progress.

So inside video model we have progress callback method:

```
  def processing_progress(format, format_options, new_progress)
    ProgressCalculator.new(self, format, format_options, new_progress).update!
  end
```

It uses `ProgressCalculator` instance to handle all this progress percentage calculation, database updates and websocket notifications:

```
class ProgressCalculator
  attr_reader :video, :format, :format_options, :new_progress

  def initialize(video, format, format_options, new_progress)
    @video = video
    @format = format
    @format_options = format_options
    @new_progress = new_progress
  end

  def update!
    update_progress_data if shoud_update?
  end

  private

  def update_progress_data
    # Had to use atomic set operation here since normal update
    # has been setting file_processing to false while processing still was not finished
    step_metadata.set(progress: new_progress.to_f)
    video.set(progress: calculate_overall_progress.to_f)
    notify_about_progress
  end

  def step
    format_options[:processing_metadata][:step]
  end

  def step_metadata
    video.processing_metadatas.find_or_create_by!(step: step) do |pm|
      pm.format = format
    end
  end

  def diff
    new_progress.to_f - step_metadata.progress.to_f
  end

  def shoud_update?
    # Update this value only each 10th percent or when processing is finished
    diff >= 0.1 || (new_progress.to_i == 1)
  end

  def steps_count
    # Normalize step + 1
    # Effects steps + effects.count
    # Watermark step + 1
    # Read video metadata step + 1
    # Generate thumbnails step + 1
    count = ::VideoUploader::OBLIGATORY_STEPS.count + video.effects.count
    count += 1 if video.watermark_image.path.present?
    count += 1 if video.needs_thumbnails?
    count
  end

  def calculate_overall_progress
    video.processing_metadatas.sum(:progress) / steps_count
  end

  def notify_about_progress
    ::ActionCable.server.broadcast(
      'notifications_channel',
      progress_payload
    )
  end

  def progress_payload
    ::CableData::VideoProcessing::ProgressSerializer.new(video).as_json
  end
end
```

Method `notify_about_progress` uses rails `ActionCable` for progress update on video details page. It would be described more later.


**6. Custom video saver worker**

We are using background workers for video processing and video storage (in production scenario this worker uploads file to cloud like Amazon S3). Event when video processing is ready happens there. So, we have to update page with processed video from this background worker.

Also we use sidekiq workers directly without rails' Active Job, since it has been failing at this storage worker. It is related to compatibility issue with new rails and this `carrierwave_backgrounder` gem.

So we redefine it's `perform` method to run custom callbacks here `run_video_processing_chain_completed_callbacks`.


```
class VideoSaverWorker < ::CarrierWave::Workers::StoreAsset
  def perform(*args)
    super(*args)
    run_video_processing_chain_completed_callbacks
  end

  private

  def run_video_processing_chain_completed_callbacks
    record.processing_completed_callback if record.respond_to?(:processing_completed_callback)
  end
end
```

`processing_completed_callback` is defined at `Video` model. It sends Action Cable websocket notification with sign that processing has been finished.

```
  def processing_completed_callback
    ::ActionCable.server.broadcast(
      'notifications_channel',
      ::CableData::VideoProcessing::CompletedSerializer.new(self).as_json
    )
  end
```

**7. ActionCable and WebSocket notifications**

For instant processing progress updates via Web Sockets we use `ActionCable`.

Use this command to generate Notifications channel `rails generate channel Notifications`.

Since we want to use redis for ActionCable websocket notifications in development environment too we have to change this config: `config / cable.yml`:

```
development:
  adapter: redis
  url: redis://localhost:6379/1
  ...
```

Since we do not have any users and authorizations all browsers which have opened our site are subscribed to `notifications_channel`.

`app/assets/javascripts/channels/notifications.coffee`:

```
App.notifications = App.cable.subscriptions.create "NotificationsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server
    console.log('Connected')

  disconnected: ->
    # Called when the subscription has been terminated by the server
    console.log('Disconnected')

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    if data['processing_completed'] == false
      $("#video_progress").replaceWith($(data['html']))
    else
      $("#video_info").replaceWith($(data['html']))
      if $('#thumbnails').length
        # Init thumbnails carousel when they are present only
        $('.carousel').carousel({})
```

After each webscoket notification it updates video processing progress information. And when processing is finished it replaces all page content with data that have been delivered by Action Cable.

This code it not the best way to this. Normally you should send JSON data over websockets and all styling and formatting should happen at frontend side. But let's skip this. This is made to just describe idea.

Channel is defined here at backend `app/channels/notifications_channel.rb`:

```
class NotificationsChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'notifications_channel'
  end
  ...
end

```

In code we have two places where we send websocket notifications from server to `notifications_channel`:

* `ProgressCalculator#notify_about_progress` during each progress update
* `Video#processing_completed_callback` when processing has been finished and file is ready.

Processing Progress Action Cable Notification uses `CableData::VideoProcessing:: ProgressSerializer`  to generate notification payload:

```
module CableData
  module VideoProcessing
    class ProgressSerializer < ActiveModel::Serializer
      attributes :html, :processing_completed

      def html
        # INFO: This is made for simplicity
        #       But in real application it is better to send
        #       JSON data via action cable only and process
        #       all styling and markup at frontend side
        ApplicationController.renderer.render(
          locals: { video: object },
          partial: 'videos/progress'
        )
      end

      def processing_completed
        false
      end
    end
  end
end
```

Processing Completed Action Cable Notification uses `CableData::VideoProcessing::CompletedSerializer` to generate notification payload:

```
module CableData
  module VideoProcessing
    class CompletedSerializer < ActiveModel::Serializer
      attributes :html, :processing_completed

      def html
        # INFO: This is made for simplicity
        #       But in real application it is better to send
        #       JSON data via action cable only and process
        #       all styling and markup at frontend side
        ApplicationController.renderer.render(
          locals: { video: object },
          partial: 'videos/info'
        )
      end

      def processing_completed
        true
      end
    end
  end
end
```

FFmpeg and frei0r filters
-----------

Read about how ffmpeg works [here](https://ffmpeg.org/ffmpeg.html#Synopsis)

All ffmpeg filters are described [here](https://ffmpeg.org/ffmpeg-filters.html#Description)

**1. Video normalization step**

All input videos are converted to mp4 format.

For initial input processing we use such `carrierwave-video` options:

*    `resolution: '500x400'` desired video resolution, by default it preserves height ratio `preserve_aspect_ratio: :height`.
*    `video_codec: 'libx264'` H.264/MPEG-4 AVC video codec
*    `constant_rate_factor: '30'` GOP Size
*    `frame_rate: '25'` frame rate
*    `audio_codec: 'aac'` AAC codec is used for audio
*    `audio_bitrate: '64k'` Audio bitrate
*    `audio_sample_rate: '44100'` Audio sampling frequency

**2. Video and audio effects**

FFmpeg with Frei0r plugin is used for video effects.

Options `-vf` and `-af` specify simple video and audio [filters](https://ffmpeg.org/ffmpeg.html#Simple-filtergraphs)

This option `-filter_complex` specifies [complex filter](https://ffmpeg.org/ffmpeg.html#Complex-filtergraphs)

To avoid excessive audio processing we copy it unchanged from input source file with this option `-c:a copy` for all video filters.

At all only audio effects we copy video steam unchanged with this option `-c:v copy`

You could read about stream copy [here](https://ffmpeg.org/ffmpeg.html#Stream-copy)

This option is not applied for `reverse`, `slow down` and `speed up` effects because they alter both audio and video streams.

By default ffmpeg selects just one best input stream of it's kind (video, audio, subtitles, etc) let's assume that for audio effects we want all input streams, for example if they are in different languages. This is achieved with `-map 0` option.

All Frei0r plugin video filters are applied with FFmpeg simple filter option `-vf frei0r=filtername:param1_value|param2_value|param3_value`

Frei0r official page [https://frei0r.dyne.org/](https://frei0r.dyne.org/) doesn't contain any documentation on how it could be used.

During research I have found this resrouce [https://www.mltframework.org/plugins/PluginsFilters/](https://www.mltframework.org/plugins/PluginsFilters/) that describes frei0r filters along with some other differect plugins. It was very helpful for me to learn about all available filters and their parameters.

Here is original video that is used for **video effects** processing demonstration

[![No Effects](https://img.youtube.com/vi/o0Vx7Fx7aWk/0.jpg)](https://www.youtube.com/watch?v=o0Vx7Fx7aWk "No Effects")

Here is original video that was used for **audio effects** demonstration

[![No Effects](https://img.youtube.com/vi/3F-Kpc-3RYo/0.jpg)](https://www.youtube.com/watch?v=3F-Kpc-3RYo "No Effects")

**2.1 Sepia effect**

Complex filter [colorchannelmixer](https://ffmpeg.org/ffmpeg-filters.html#colorchannelmixer) adjusts video input frames by re-mixing color channels.

`-filter_complex colorchannelmixer=.393:.769:.189:0:.349:.686:.168:0:.272:.534:.131` this filter changes color temperature to be sepia like

[![Sepia Effect](https://img.youtube.com/vi/3eJzZP7KWXo/0.jpg)](https://www.youtube.com/watch?v=3eJzZP7KWXo "Sepia Effect")


**2.2 Black and White effect**

Zero saturation with [hue](https://ffmpeg.org/ffmpeg-filters.html#hue) filter `-vf hue=s=0` produces black and white output video.

[![Black and White Effect](https://img.youtube.com/vi/wTELftdZaB0/0.jpg)](https://www.youtube.com/watch?v=wTELftdZaB0 "Black and White Effect")

**2.3 Vertigo effect**

[Vertigo](https://www.mltframework.org/plugins/FilterFrei0r-vertigo/) effect `-vf frei0r=vertigo:0.2` performas alpha blending with zoomed and rotated images. I set `phase increment` parameter to 0.2 and keep `zoomrate` parameter with default value.

[![Vertigo Effect](https://img.youtube.com/vi/RtyqzyCEyfc/0.jpg)](https://www.youtube.com/watch?v=RtyqzyCEyfc "Vertigo Effect")


**2.4 Vignette effect**

[Vignette](https://www.mltframework.org/plugins/FilterFrei0r-vignette/) effect `-vf frei0r=vignette` performs lens vignetting effect. Here it is used with default settings.

[![Vignette Effect](https://img.youtube.com/vi/QuPksoLNlTs/0.jpg)](https://www.youtube.com/watch?v=QuPksoLNlTs "Vignette Effect")

**2.5 Sobel effect**

[Sobel Effect](https://www.mltframework.org/plugins/FilterFrei0r-sobel/)
is used in image processing and computer vision, particularly within edge detection algorithms where it creates an image emphasising edges. [Read more](https://en.wikipedia.org/wiki/Sobel_operator)

It doesn't have any parameters: `-vf frei0r=sobel`

[![Sobel Effect](https://img.youtube.com/vi/ppeLcRPzlOM/0.jpg)](https://www.youtube.com/watch?v=ppeLcRPzlOM "Sobel Effect")

**2.6 Pixelizor effect**

[Pixelizor Effect](https://www.mltframework.org/plugins/FilterFrei0r-pixeliz0r/)
is applied with default settings. It creates pixelated video.

`-vf frei0r=pixeliz0r`

[![Pixelizor Effect](https://img.youtube.com/vi/61PwT7oPF-s/0.jpg)](https://www.youtube.com/watch?v=61PwT7oPF-s "Pixelizor Effect")


**2.7 Invertor effect**

[Invertor Effect](https://www.mltframework.org/plugins/FilterFrei0r-invert0r/)
inverts all colors of a source image

`-vf frei0r=invert0r`

[![Invertor Effect](https://img.youtube.com/vi/ai69LNPhB9Y/0.jpg)](https://www.youtube.com/watch?v=ai69LNPhB9Y "Invertor Effect")

**2.8 RGBnoise effect**

[RGBnoise Effect](https://www.mltframework.org/plugins/FilterFrei0r-rgbnoise/)
 it adds RGB noise to image. It has one parameter that defines amount of noise added.

`-vf frei0r=rgbnoise:0.2`

Result looks like some 90-es movie played with old video cassette player.

[![RGBnoise Effect](https://img.youtube.com/vi/TTRhpRPiKlE/0.jpg)](https://www.youtube.com/watch?v=TTRhpRPiKlE "RGBnoise Effect")

**2.9 Distorter effect**

[Distorter Effect](https://www.mltframework.org/plugins/FilterFrei0r-distort0r/)
distorts image in a weird way.

I set here two parameters: amplitude and frequency of the plasma signal.

`-vf frei0r=distort0r:0.05|0.0000001`

[![Distorter Effect](https://img.youtube.com/vi/w6Nk9oobXQE/0.jpg)](https://www.youtube.com/watch?v=w6Nk9oobXQE "Distorter Effect")

**2.10 IIRblur effect**

[IIRblur Effect](https://www.mltframework.org/plugins/FilterFrei0r-iirblur/)
provides Infinite Impulse Response Gaussian blur.

`-vf frei0r=iirblur` I use it with default settings.

[![IIRblur Effect](https://img.youtube.com/vi/qOoF6eCk6rA/0.jpg)](https://www.youtube.com/watch?v=qOoF6eCk6rA "IIRblur Effect")

**2.11 Nervous effect**

[Nervous Effect](https://www.mltframework.org/plugins/FilterFrei0r-nervous/)
it flushes frames in time in a nervous way.

Maybe this filter is not recommended for people with some neurological disorders like epilepsy.

`-vf frei0r=nervous` It doesn't have any parameters.

[![Nervous Effect](https://img.youtube.com/vi/3UZSFLNvp8s/0.jpg)](https://www.youtube.com/watch?v=3UZSFLNvp8s "Nervous Effect")

**2.12 Glow effect**

[Glow Effect](https://www.mltframework.org/plugins/FilterFrei0r-glow/) creates a Glamorous Glow.

`-vf frei0r=glow:1` `Blur of the glow` parameter is set to to maximum value.

[![Glow Effect](https://img.youtube.com/vi/BKLDN_5foUE/0.jpg)](https://www.youtube.com/watch?v=BKLDN_5foUE "Glow Effect")

**2.13 Reverse effect**

It is made with two FFmpeg filters: video reverse and audio reverse at the same time.

`-vf reverse -af areverse`

[![Reverse Effect](https://img.youtube.com/vi/fPQTDrH13GA/0.jpg)](https://www.youtube.com/watch?v=fPQTDrH13GA "Reverse Effect")

**2.14 Slow Down effect**

`-filter:v setpts=2.0*PTS -filter:a atempo=0.5`

It uses [setpts](http://ffmpeg.org/ffmpeg-all.html#setpts_002c-asetpts) filter to double [Presentation Time Stamp (PTS)](https://en.wikipedia.org/wiki/Presentation_timestamp) of original video. This results in slowing video in two times.

[atempo](http://ffmpeg.org/ffmpeg-all.html#atempo) filter is used to slow down audio in two times with 50% of original tempo setting.

[![Slow Down Effect](https://img.youtube.com/vi/f4KvxrxMKMs/0.jpg)](https://www.youtube.com/watch?v=f4KvxrxMKMs "Slow Down")

[Read more](https://trac.ffmpeg.org/wiki/How%20to%20speed%20up%20/%20slow%20down%20a%20video)

**2.15 Speed Up effect**

`-filter:v setpts=0.5*PTS -filter:a atempo=2.0` works with the same approach as slow down effect but vise versa. It takes half of original video Presentation Time Stamp and increases video tempo in two times.

[![Speed Up Effect](https://img.youtube.com/vi/SGDrpo0HIL8/0.jpg)](https://www.youtube.com/watch?v=SGDrpo0HIL8 "Speed Up Effect")

**2.16 Echo Audio effect**

[aecho](https://ffmpeg.org/ffmpeg-filters.html#aecho) audio filter is used here

`-af aecho=0.8:0.9:1000|500:0.7|0.5` first parameter is input gain of reflected signal. Second parameter is output gain of reflected signal. Next it has list of  delays from original signal in milliseconds separated by pipe `|` symbol. After this parameter goes list of decays for each delay in previous list. Colon sign `:` separates items in lists.


[![Echo Effect](https://img.youtube.com/vi/z4b7B-ncJC0/0.jpg)](https://www.youtube.com/watch?v=z4b7B-ncJC0 "Echo Effect")

**2.17 Tremolo Audio effect**

[tremolo](https://ffmpeg.org/ffmpeg-filters.html#tremolo) filter performs sinusoidal amplitude modulation.

`-af tremolo=f=10.0:d=0.7`

`f` parameter is modulation frequency in Hertz.

`d` parameter is depth of modulation in percentage.

[![Tremolo Effect](https://img.youtube.com/vi/dnAZuxCGZs8/0.jpg)](https://www.youtube.com/watch?v=dnAZuxCGZs8 "Tremolo Effect")

**2.18 Vibrato Audio effect**

[vibrato](https://ffmpeg.org/ffmpeg-filters.html#vibrato) filter performs sinusoidal phase modulation.

`-af vibrato=f=7.0:d=0.5`

`f` parameter is modulation frequency in Hertz.

`d` parameter is depth of modulation in percentage.

[![Vibrato Effect](https://img.youtube.com/vi/MoJEZoNTpek/0.jpg)](https://www.youtube.com/watch?v=MoJEZoNTpek "Vibrato Effect")

**2.19 Chorus Audio effect**

[chorus](https://ffmpeg.org/ffmpeg-filters.html#chorus) filter resembles an echo effect with a short delay, but whereas with echo the delay is constant, with chorus, it is varied using using sinusoidal or triangular modulation.

`-af chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3`

It's parameters are ordered in such way: Input Gain, Output Gain, Delays List in ms, Decays List, Speeds List, Depths List.

Parameters are separated by pipe sign `|`, colon sign `:` separates items in lists.

[![Chorus Effect](https://img.youtube.com/vi/gT67tOFMi2w/0.jpg)](https://www.youtube.com/watch?v=gT67tOFMi2w "Chorus Effect")

**2.20. Watermark**

Watermark feature is implemented inside `carrierwave-video` gem. It uses FFmpeg's [overlay](https://ffmpeg.org/ffmpeg-filters.html#overlay-1) filter

Here is combination of such effects: *Vertigo, Vignette, Rgbnoise, Distorter, Glow, Reverse, Echo, Tremolo, Chorus, Watermark.*

Here watermark is transparent png image with red text **'FFmpeg TV'**:

[![Watermark plus many Effects](https://img.youtube.com/vi/sEnBu2VfMJo/0.jpg)](https://www.youtube.com/watch?v=sEnBu2VfMJo "Watermark plus many Effects")


System resources usage control
-----------

FFmpeg video processing is heavy operation that usually takes all server CPU resources (Almost 100%). This might slow down some more important operations like web servers, databases, etc

There are few solutions to deal with it: limit FFmpeg CPU Usage with some linux system tools and FFmpeg configuration flags, run processing inside docker container with limited resources, separate video processing into some service that runs at different machine than primary server.

**1. Limit CPU usage with system tools**

Alter ffmpeg system priority with linux [`nice`](http://man7.org/linux/man-pages/man1/nice.1.html) command during process start-up. Or alter priority of already runnig process with [`renice`](http://manpages.ubuntu.com/manpages/zesty/en/man1/renice.1.html)

Highest priority is -20 lowest possible priority is 19. This might help saving CPU for more important processes.

Next you can use [`cpulimit`](http://cpulimit.sourceforge.net/) when you want to ensure that a process doesn't use more than a certain portion of the CPU. The disadvantage over `nice` is that the process can't use all of the available CPU time when the system is idle.

Here is my CPU load without FFmpeg or any other heavy operations:

![System Idle](https://content.screencast.com/users/alexius-s/folders/Jing/media/754172f1-d1ab-4af7-8174-7dfc214d5b7a/00000114.png)

Small 15 second video with one video and one audio filter have been processed 28 seconds on my 4 Core Macbook Air with 100% CPU Load

`time ffmpeg -i test_video.mp4 -vf frei0r=vertigo:0.2 -af "chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3" -vcodec h264 -acodec aac -strict -2 video_with_filters.mp4`

`93.45s user 0.88s system 333% cpu 28.285 total` was processed in 28 seconds with 100% CPU load

![No limitations](https://content.screencast.com/users/alexius-s/folders/Jing/media/f7db6d15-0a3e-4e5f-8b94-224fedf42f57/00000112.png)

With 50% CPU Limit

`time cpulimit --limit 50 ffmpeg -i test_video.mp4 -vf frei0r=vertigo:0.2 -af "chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3" -vcodec h264 -acodec aac -strict -2 video_with_filters_cpulimit_50.mp4`

`103.44s user 1.57s system 50% cpu 3:26.60 total` It took almost 3.5 minutes to be completed but CPU was always loaded no more than 50%

![50% CPU Limit](https://content.screencast.com/users/alexius-s/folders/Jing/media/4b63cc1b-6056-4125-8224-3d8f4ced372f/00000113.png)

Also FFMpeg has `-threads` option that limits number of used threads (CPU cores). Recommended value is number of CPU cores minus one or two. Default value is all available CPU Cores. Note that this option always should be added as last parameter (before output file name).

`time ffmpeg -i test_video.mp4 -vf frei0r=vertigo:0.2 -af "chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3" -vcodec h264 -acodec aac -strict -2 -threads 1 video_with_filters_one_thread.mp4`

`60.02s user 1.48s system 99% cpu 1:02.04 total` It took 1 minute to complete this operation. CPU Load was not so heavy also

![FFmpeg one thread](https://content.screencast.com/users/alexius-s/folders/Jing/media/623576c0-4b48-4b52-9a84-8b94cc8a61d2/00000115.png)

Read more about restricting Linux system resources [here](http://blog.scoutapp.com/articles/2014/11/04/restricting-process-cpu-usage-using-nice-cpulimit-and-cgroups)

Also you could use all these options at the [same time](https://lorenzo.mile.si/2016/07/limiting-cpu-usage-and-server-load-with-ffmpeg/).

If you are using the same gems as at this example app: `streamio-ffmpeg` and `carrierwave-video` than you could specify `-threads` parameter as [custom option](https://github.com/streamio/streamio-ffmpeg#transcoding) the same way as it was used for filters in our application.

As for `nice` and `cpulimit` linux commands you could alter default FFmpeg command path at [`streamio-ffmpeg`](https://github.com/streamio/streamio-ffmpeg#specify-the-path-to-ffmpeg) gem at some initializer file (`config/initializers`).

Orignal [`streamio-ffmpeg`](https://github.com/streamio/streamio-ffmpeg/blob/master/lib/streamio-ffmpeg.rb#L40) gem has check that verifies if ffmpeg binary is [executable](https://github.com/streamio/streamio-ffmpeg/blob/master/lib/streamio-ffmpeg.rb#L40).

This would not let you set this parameter directly with this `'/usr/local/bin/cpulimit --limit 50 /usr/local/bin/ffmpeg'`.

So there are two ways to deal with it: redefine `FFMPEG.ffmpeg_binary=(bin)` method to remove this check or create executable bash script. Since monkeypatching gem's code is a bad practice let's create bash script with such code:

`/usr/local/bin/cpulimit --limit 50 /usr/local/bin/ffmpeg "$@"`

This parameter `"$@"` would pass all .sh script arguments to ffmpeg command.

Let's put it at project's root folder. Do not forget to allow it be executable:

`chmod +x ffmpeg_cpulimit.sh`

Now you could set path to your script at initializer:

`FFMPEG.ffmpeg_binary = "#{::Rails.root}/ffmpeg_cpulimit.sh"`


Also current sidekiq configuration `config/sidekiq.yml` could process 3 tasks simultaneously. If you also want to limit system load you have to set `concurrency` to `1` to run only one processing task at a time.

```
:verbose: true
:pidfile: ./tmp/pids/sidekiq.pid
:logfile: ./log/sidekiq.log
:concurrency: 3
:queues:
  - carrierwave
```

**2. FFmpeg inside Docker**

Another option is running FFmpeg commands inside docker. It is possible to limit how much resources a single ffmpeg instance may consume. Also this gives you ability to control not only CPU usage but also memory and IOs.

Let's try it with this predefined ffmpeg image from github: [https://github.com/jrottenberg/ffmpeg](https://github.com/jrottenberg/ffmpeg)

I have altered command to avoid using frei0r filter since this ffmpeg image doesn't have frei0r plugin

```
time docker run --rm -v `pwd`:/tmp/workdir -w="/tmp/workdir" jrottenberg/ffmpeg -i test_video.mp4 -filter_complex colorchannelmixer=.393:.769:.189:0:.349:.686:.168:0:.272:.534:.131 -af "chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3" -vcodec h264 -acodec aac -strict -2 test_video_docker.mp4
```

`0.02s user 0.02s system 0% cpu 33.632 total` operation has been completed in 33 seconds.

With default configuration my CPU load was not 100% for all cores also:

![FFmpeg With Docker](https://content.screencast.com/users/alexius-s/folders/Jing/media/403ddc9b-ad7d-4d6d-8125-8813cafb3c61/00000120.png)

Read more here
[https://docs.docker.com/engine/reference/run/#runtime-constraints-on-resources](https://docs.docker.com/engine/reference/run/#runtime-constraints-on-resources)

And of course you would have to alter `streamio-ffmpeg` gem's code to use this command with docker if you wan't to use it with these gems.

**3. Separate video processing service**

If you have enough resources to run heavy background operations at dedicated server than it might be the best option.

You could create video processing service with some API and callbacks for different processing events like processing finished or failed, or with websocket processing progress notifications.

This requires more developer work to create custom separated service, create it's integration with primary application server (that runs some business logic), but it is much more better with performance and reliability than sharing one machine between business logic server and background job workers.

Here is very simple example of such background jobs application [Video Processor](https://github.com/suhovius/video_processor) app. It could process only one job and it doesn't have callbacks. It has very simple API.

Conclusions
-----------
FFmpeg video processing is heavy operation that should be performed at background. Ruby on Rails ecosystem has all needed features:

1. Background job processors (`sidekiq`, `resque`)
2. Background file storage gems (`carrierwave_backgrounder`)
3. Video processing gems (`streamio-ffmpeg`, `carrierwave-video`, `carrierwave-video-thumbnailer`)

By default FFmpeg takes almost all CPU power that may slow down other critical operations on server machine. There are few approaches to deal with this problem:

1. Limit used resources with linux system tools like `nice`, `cpulimit` to limit system priority and cpu usage.
2. Use FFmpeg's `-threads` command option to limit number of threads that ffmpeg is using for video processing operations.
3. Isolate all video processing environments from primary operating system into fully controllable environment with docker.
4. Move all video processing logic from main application to some video processing service that is running on other separated machine.

FFMpeg is very powerful tool, that is capable to record, convert and stream audio and video.

We have covered just some set of it's features like:

1. Video and audio formats conversion,
2. Video and audio filtering with native FFmpeg filters
3. Applying cool video filters from Frei0r plugin
4. Taking screenshots from video
5. Reading video metadata
