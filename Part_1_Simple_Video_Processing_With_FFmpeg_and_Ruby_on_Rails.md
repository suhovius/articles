Suggested Metatags: ffmpeg, ffmpegthumbnailer, video processing, sidekiq, carrierwave_backgrounder, carrierwave, streamio-ffmpeg, ruby, rails


Article #1: Simple Video Processing with FFmpeg and Ruby on Rails. FFmpeg system usage limitation.
=============


Introduction
-------------

In this article I am going to describe possibilities of standard gems for video processing with Ruby on Rails, plus FFmpeg performance testing and optimization tweaks.

Videos are going to be processed in background with [sidekiq](https://github.com/mperham/sidekiq) gem.

You could use almost any database with video processing in Rails. In this project I am going to use NoSQL database [MongoDB](https://www.mongodb.com/)

All video processing is going to happen with [FFmpeg](https://ffmpeg.org/) utility.

This is first introductory article about video processing with Rails. Second article would cover more advanced things like video filters, reading video metadata, making screenshots for each second of video, and real time monitoring of video processing progress. **TODO Add link to second article**

You could read about experiments with video processing filters at third article **TODO Add link to third article**

TL;DR
-------------
If you do not want to read all this article than project repository is here:
[https://github.com/Yalantis/video-manipulator](https://github.com/Yalantis/video-manipulator)

Project Setup
-------------

**1. Install FFmpeg with all options**

If your operating system is OSX than you could use homebrew for installation

```
brew install ffmpeg --with-fdk-aac --with-frei0r --with-libvo-aacenc --with-libvorbis --with-libvpx --with-opencore-amr --with-openjpeg --with-opus --with-schroedinger --with-theora --with-tools
```

Maybe not all of these options are needed for current project but I included almost all possible to avoid issues when something is not working if some ffmpeg extension has not been installed. Since I have been planning to experiment with different ffmpeg extensions.

On other Linux OS like Ubuntu or Debian it might require to install ffmpeg from sources. Maybe this guide could be helpful [https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu](https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu)

**2. Install ffmpeg thumbnailer**

`brew install ffmpegthumbnailer` this utility would be used for video thumbnail creation.

**3. Install imagemagick**

If want to work with generated thumbnail image or with uploaded watermark image than you will need `imagemagick` to be installed.

`brew install imagemagick`

**4. Install MongoDB, Redis**

`brew install mongodb` project's database

`brew install redis` storage for sidekiq job parameters and data

**5. Ruby and Rails**

I assume that you already have rvm (ruby version manager), ruby and rails installed so I am not going to cover this here.

JFYI: I have been using `ruby 2.3.0` and `Rails 5.0.1` for this project.

Development
-----------

**1. Install and configure gems**

First create new rails project without default active record, since we are going to use mongoDB for data storage.

`rails new video_manipulator --skip-active-record`

Than add all needed gems to Gemfile

```
# Video Processing
gem 'streamio-ffmpeg'
gem 'carrierwave-video', github: 'evgeniy-trebin/carrierwave-video'
gem 'carrierwave-video-thumbnailer',
    github: '23shortstop/carrierwave-video-thumbnailer'
gem 'carrierwave_backgrounder', github: '23shortstop/carrierwave_backgrounder'
gem 'kaminari-mongoid', '~> 0.1.0'

# Data Storage (Order matters here. It should be required after carrierwave-video)
gem 'mongoid', '~> 6.1.0'
gem 'carrierwave-mongoid', require: 'carrierwave/mongoid'

# Background jobs
gem 'sidekiq'

# Web site styles
gem 'materialize-sass'

# This would be used for JSON serialization
gem 'active_model_serializers', '~> 0.10.0'
```

As you can see some of these gems are installed directly from github without rubygems. In general it is not always good to install gems like this, but these ones have some fixes and some more contemporary changes, since originals of these gems at [https://rubygems.org/](https://rubygems.org/) have been updated a few years ago last time.

Install these gems with `bundle install` command.

Configure these gems inside project. Read about these gems configuration here:

* [https://github.com/23shortstop/carrierwave_backgrounder](https://github.com/23shortstop/carrierwave_backgrounder)
* [https://docs.mongodb.com/mongoid/master/tutorials/mongoid-installation/](https://docs.mongodb.com/mongoid/master/tutorials/mongoid-installation/)
* [https://github.com/mkhairi/materialize-sass](https://github.com/mkhairi/materialize-sass) this gem is used for styles. You might use some other styles bootstrap gem. Or create your own html markup styling.

Note that due compatibility with `carrierwave_backgrounder` we are using sidekiq workers, not modern active job workers from rails.

So, inside `config/initializers/carrierwave_backgrounder.rb`
sidekiq is configured as backend for this video processing and uploading queues

```
CarrierWave::Backgrounder.configure do |c|
  c.backend :sidekiq, queue: :carrierwave
end

```

For `mongoid` gem we just generate it's generic config.

`rails g mongoid:config`

It is enough for this demo research app. Of course for production/commercial apps you should not store database config file at project's repository since it might contain some database connection credentials. The same relies for mongoDB since by default installation it doesn't have any password for database connection, so in production you have to set up password for mongoDB too.

**2. Generate basic scaffold for Videos**

We are not going to add authorization, users, etc. Since it is just demo application for videos processing with Rails.

So, let’s generate scaffold for videos with this command:

`rails generate scaffold Video title:string file:string file_tmp:string file_processing:boolean watermark_image:string`

In order to store and process files in background our model requires these attributes:

`file_tmp:string` this would store uploaded file temporarily

`file_processing:boolean` this could let us check when processing has been finished. It would be updated by `carrierwave_backgrounder` gem.

**3. Generate Video Uploader**

This command is used here: `rails generate uploader Video`

Let's include needed modules from previously installed gems at newly generated `VideoUploader`:

```
  # Store and process video in the background
  include ::CarrierWave::Backgrounder::Delay
  # Use carrierwave-video gem's methods here
  include ::CarrierWave::Video
  # Use carrierwave-video-thumbnailer gem's methods here
  include ::CarrierWave::Video::Thumbnailer
```

Than define processing parameters for video encoding:


```
  PROCESSED_DEFAULTS = {
    resolution:           '500x400', # desired video resolution, by default it preserves height ratio `preserve_aspect_ratio: :height`.
    video_codec:          'libx264', # H.264/MPEG-4 AVC video codec
    constant_rate_factor: '30', # GOP Size
    frame_rate:           '25', # frame rate
    audio_codec:          'aac', # AAC codec is used for audio
    audio_bitrate:        '64k', # Audio bitrate
    audio_sample_rate:    '44100' # Audio sampling frequency
  }.freeze
```

Also you could define acceptable video file formats:

```
  def extension_whitelist
    %w[mov mp4 3gp mkv webm m4v avi]
  end
```

Next you have to specify where uploaded files should be stored

```
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end
```

The main part is encoding operation. We do not need to keep original file that's why processing is forced ower original file. So, when processing is finished, processed file would replace original one. And also this would ensure that thumbnail would be generated from processed file.

Also during processing we use `:watermark` option from `carrierwave-video` gem to overlay user provided image from `watermark_image` field at `Video` model.

Here is the main video processing code:

```
  process encode: [:mp4, PROCESSED_DEFAULTS]

  def encode(format, opts = {})
    encode_video(format, opts) do |_, params|
	  if model.watermark_image.path.present?
        params[:watermark] ||= {}
        params[:watermark][:path] = model.watermark_image.path
      end
    end
  end
```

In our case it also uses `carrierwave-video-thumbnailer` gem for video thumbnail image generation from the middle (50%) of the file. Note that there is code that ensures setting proper content type for generated thumbnail, since by default it would use video file content type and extension.
Thumbnail is processed like different version of uploaded attachment with `version` definition:

```
  version :thumb do
    process thumbnail: [
      { format: 'png', quality: 10, size: 200, seek: '50%', logger: Rails.logger }
    ]

    def full_filename(for_file)
      png_name for_file, version_name
    end

    process :apply_png_content_type
  end

  def png_name(for_file, version_name)
    %(#{version_name}_#{for_file.chomp(File.extname(for_file))}.png)
  end

  def apply_png_content_type(*)
    file.instance_variable_set(:@content_type, 'image/png')
  end
end
```

You could read more about this solution here [https://github.com/evrone/carrierwave-video-thumbnailer/issues/6#issuecomment-28664696](https://github.com/evrone/carrierwave-video-thumbnailer/issues/6#issuecomment-28664696)

Inside `Video` model we have to define all needed attributes:

```
  # file_tmp is used for temporary file saving while
  # it processed and stored in the background by carrierwave_backgrounder gem
  field :file_tmp, type: String
  # file_processing attribute is managed by carrierwave_backgrounder
  # it contains sign of uploaded file state. Is under processing or not.
  field :file_processing, type: Boolean
```

And `mount_uploader` for `file` attribute that uses `VideoUploader`:

```
  # mount_on is specified here because without it gem
  # would name filename attribute as file_filename
  # In some way it looks logical. But in our case it is strange to have
  # file_filename attribute at database
  mount_uploader :file, ::VideoUploader, mount_on: :file
```

Here we specify that we want our attachment to be processed and stored in background:

```
  process_in_background :file
  store_in_background :file
  validates_presence_of :file
```

This is especially useful when file is processed and stored at some external storage like Amazon S3. This might be long process and it's good to let it be performed in background. Since in other case user have to wait until processing and upload are finished, which also might exceed server request timeout.

Also we have to add uploader mount for watermark image:

```
  # The same as above. We do not want to have watermark_image_filename attribute
  mount_uploader :watermark_image, ::ImageUploader, mount_on: :watermark_image
```

Let's add uploader for our `watermark_image`. Run this command `rails generate uploader Image`. Here is our image uploader:

```
class ImageUploader < CarrierWave::Uploader::Base
  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_whitelist
    %w[jpg jpeg gif png]
  end
end
```

Do not forget to add sidekiq configuration here: `config/sidekiq.yml`:

```
:verbose: true
:pidfile: ./tmp/pids/sidekiq.pid
:logfile: ./log/sidekiq.log
:concurrency: 3
:queues:
  - carrierwave
```

Main part is `queues` key, since `carrierwave` is queue where all video processing tasks are goin be stored.


We are not going to cover html views part. Mainly generated scaffold should be enough. Just one thing that you might need to add is `watermark_image` attribute at video creation form.

This is how video upoloading from might look without any additional markup:

```
<%= form_for(video) do |f| %>
  <%= f.text_field :title, placeholder: 'Title' %>
  <%= f.file_field :file, placeholder: 'Video File' %>
  <%= f.file_field :watermark_image, placeholder: 'Watermark Image' %>
  <%= f.submit %>
<% end %>
```

Than in video details view you could use `video_tag` rails helper method to display html5 video player

`<%= video_tag(video.file.url, controls: true) if video.file.url.present? %>`


Now, you just have to run background job:

`bundle exec sidekiq -C config/sidekiq.yml`

and your server: `rails s`

That it! You have simple video processing server made just in 15 minutes.


System resources usage control
-----------

FFmpeg video processing is heavy operation that usually takes all server CPU resources (Almost 100%). This might slow down some more important operations like web servers, databases, etc

There are few solutions to deal with it: limit FFmpeg CPU Usage with some linux system tools and FFmpeg configuration flags, run processing inside docker container with limited resources, separate video processing into some service that runs at different machine than primary server.

Also note that in this section we are using video and audio filters which would be covered in second part of this article

**1. Limit CPU usage with system tools**

Alter ffmpeg system priority with linux [`nice`](http://man7.org/linux/man-pages/man1/nice.1.html) command during process start-up. Or alter priority of already runnig process with [`renice`](http://manpages.ubuntu.com/manpages/zesty/en/man1/renice.1.html)

Highest priority is -20 lowest possible priority is 19. This might help saving CPU for more important processes.

Next you can use [`cpulimit`](http://cpulimit.sourceforge.net/) when you want to ensure that a process doesn't use more than a certain portion of the CPU. The disadvantage over `nice` is that the process can't use all of the available CPU time when the system is idle.

Here is my CPU load without FFmpeg or any other heavy operations:

![System Idle](https://content.screencast.com/users/alexius-s/folders/Jing/media/754172f1-d1ab-4af7-8174-7dfc214d5b7a/00000114.png)

Small 15 second video with one video and one audio filter have been processed 28 seconds on my 4 Core Macbook Air with 100% CPU Load

`time ffmpeg -i test_video.mp4 -vf frei0r=vertigo:0.2 -af "chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3" -vcodec h264 -acodec aac -strict -2 video_with_filters.mp4`

`93.45s user 0.88s system 333% cpu 28.285 total` was processed in 28 seconds with 100% CPU load

![No limitations](https://content.screencast.com/users/alexius-s/folders/Jing/media/f7db6d15-0a3e-4e5f-8b94-224fedf42f57/00000112.png)

With 50% CPU Limit

`time cpulimit --limit 50 ffmpeg -i test_video.mp4 -vf frei0r=vertigo:0.2 -af "chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3" -vcodec h264 -acodec aac -strict -2 video_with_filters_cpulimit_50.mp4`

`103.44s user 1.57s system 50% cpu 3:26.60 total` It took almost 3.5 minutes to be completed but CPU was always loaded no more than 50%

![50% CPU Limit](https://content.screencast.com/users/alexius-s/folders/Jing/media/4b63cc1b-6056-4125-8224-3d8f4ced372f/00000113.png)

Also FFMpeg has `-threads` option that limits number of used threads (CPU cores). Recommended value is number of CPU cores minus one or two. Default value is all available CPU Cores. Note that this option always should be added as last parameter (before output file name).

`time ffmpeg -i test_video.mp4 -vf frei0r=vertigo:0.2 -af "chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3" -vcodec h264 -acodec aac -strict -2 -threads 1 video_with_filters_one_thread.mp4`

`60.02s user 1.48s system 99% cpu 1:02.04 total` It took 1 minute to complete this operation. CPU Load was not so heavy also

![FFmpeg one thread](https://content.screencast.com/users/alexius-s/folders/Jing/media/623576c0-4b48-4b52-9a84-8b94cc8a61d2/00000115.png)

Read more about restricting Linux system resources [here](http://blog.scoutapp.com/articles/2014/11/04/restricting-process-cpu-usage-using-nice-cpulimit-and-cgroups)

Also you could use all these options at the [same time](https://lorenzo.mile.si/2016/07/limiting-cpu-usage-and-server-load-with-ffmpeg/).

If you are using the same gems as at this example app: `streamio-ffmpeg` and `carrierwave-video` than you could specify `-threads` parameter as [custom option](https://github.com/streamio/streamio-ffmpeg#transcoding) the same way as it was used for filters in our application.

As for `nice` and `cpulimit` linux commands you could alter default FFmpeg command path at [`streamio-ffmpeg`](https://github.com/streamio/streamio-ffmpeg#specify-the-path-to-ffmpeg) gem at some initializer file (`config/initializers`).

Orignal [`streamio-ffmpeg`](https://github.com/streamio/streamio-ffmpeg/blob/master/lib/streamio-ffmpeg.rb#L40) gem has check that verifies if ffmpeg binary is [executable](https://github.com/streamio/streamio-ffmpeg/blob/master/lib/streamio-ffmpeg.rb#L40).

This would not let you set this parameter directly with this `'/usr/local/bin/cpulimit --limit 50 /usr/local/bin/ffmpeg'`.

So there are two ways to deal with it: redefine `FFMPEG.ffmpeg_binary=(bin)` method to remove this check or create executable bash script. Since monkeypatching gem's code is a bad practice let's create bash script with such code:

`/usr/local/bin/cpulimit --limit 50 /usr/local/bin/ffmpeg "$@"`

This parameter `"$@"` would pass all .sh script arguments to ffmpeg command.

Let's put it at project's root folder. Do not forget to allow it be executable:

`chmod +x ffmpeg_cpulimit.sh`

Now you could set path to your script at initializer:

`FFMPEG.ffmpeg_binary = "#{::Rails.root}/ffmpeg_cpulimit.sh"`


Also current sidekiq configuration `config/sidekiq.yml` could process 3 tasks simultaneously. If you also want to limit system load you have to set `concurrency` to `1` to run only one processing task at a time.

```
:verbose: true
:pidfile: ./tmp/pids/sidekiq.pid
:logfile: ./log/sidekiq.log
:concurrency: 3
:queues:
  - carrierwave
```

**2. FFmpeg inside Docker**

Another option is running FFmpeg commands inside docker. It is possible to limit how much resources a single ffmpeg instance may consume. Also this gives you ability to control not only CPU usage but also memory and IOs.

Let's try it with this predefined ffmpeg image from github: [https://github.com/jrottenberg/ffmpeg](https://github.com/jrottenberg/ffmpeg)

I have altered command to avoid using frei0r filter since this ffmpeg image doesn't have frei0r plugin

```
time docker run --rm -v `pwd`:/tmp/workdir -w="/tmp/workdir" jrottenberg/ffmpeg -i test_video.mp4 -filter_complex colorchannelmixer=.393:.769:.189:0:.349:.686:.168:0:.272:.534:.131 -af "chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3" -vcodec h264 -acodec aac -strict -2 test_video_docker.mp4
```

`0.02s user 0.02s system 0% cpu 33.632 total` operation has been completed in 33 seconds.

With default configuration my CPU load was not 100% for all cores also:

![FFmpeg With Docker](https://content.screencast.com/users/alexius-s/folders/Jing/media/403ddc9b-ad7d-4d6d-8125-8813cafb3c61/00000120.png)

Read more here
[https://docs.docker.com/engine/reference/run/#runtime-constraints-on-resources](https://docs.docker.com/engine/reference/run/#runtime-constraints-on-resources)

And of course you would have to alter `streamio-ffmpeg` gem's code to use this command with docker if you want to use it with these gems.

**3. Separate video processing service**

If you have enough resources to run heavy background operations at dedicated server than it might be the best option.

You could create video processing service with some API and callbacks for different processing events like processing finished or failed, or with websocket processing progress notifications.

This requires more developer work to create custom separated service, create it's integration with primary application server (that runs some business logic), but it is much more better with performance and reliability than sharing one machine between business logic server and background job workers.

Here is very simple example of such background jobs application [https://github.com/Yalantis/Video-Processor](https://github.com/Yalantis/Video-Processor) app. It could process only one task, - video trimming, and it doesn't have any callbacks. But has very simple API.

Conclusions
-----------
FFmpeg video processing is heavy operation that should be performed at background. Ruby on Rails ecosystem has all needed features:

1. Background job processors (`sidekiq`, `resque`)
2. Background file storage gems (`carrierwave_backgrounder`)
3. Video processing gems (`streamio-ffmpeg`, `carrierwave-video`, `carrierwave-video-thumbnailer`)

By default FFmpeg takes almost all CPU power that may slow down other critical operations on server machine. There are few approaches to deal with this problem:

1. Limit used resources with linux system tools like `nice`, `cpulimit` to limit system priority and cpu usage.
2. Use FFmpeg's `-threads` command option to limit number of threads that ffmpeg is using for video processing operations.
3. Isolate all video processing operations from primary operating system into fully controllable environment with docker.
4. Move all video processing logic from main application to some video processing service that is running on other separated machine.

FFmpeg is very powerful tool, that is capable to record, convert and stream audio and video.
In this article we have covered just small part of it's capabilities:

* Video encoding. Conversion from one format to another.
* Video watermarking. (`overlay` filter from FFmpeg)