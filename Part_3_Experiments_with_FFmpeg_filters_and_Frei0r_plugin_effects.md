Suggested Metatags: ffmpeg, frei0r, video effects, sound effects, video processing

**Article #3: Experiments with FFmpeg filters and Frei0r plugin effects**
=============

Introduction
-------------

This is third article about video processing with Rails. Here I am going to show examples of FFmpeg video and audio filters and Frei0r plugin effects. All these features have been added to application that has been developed in second and first articles.

**TODO Add link to second article**

**TODO Add link to first article**

TL;DR

If you do not want to read all this article than project repository is here:
[https://github.com/Yalantis/video-manipulator](https://github.com/Yalantis/video-manipulator)

FFmpeg Installation
--------------------

If your operating system is OSX than you could use homebrew for installation

```
brew install ffmpeg --with-fdk-aac --with-frei0r --with-libvo-aacenc --with-libvorbis --with-libvpx --with-opencore-amr --with-openjpeg --with-opus --with-schroedinger --with-theora --with-tools
```

Maybe not all of these options are needed for current project but I included almost all possible to avoid issues when something is not working if some extension has not been installed.

Fix frei0r
----------

When I installed ffmpeg with Frei0r, it's effects were not working. To check if you have such issue run ffmpeg with frei0r effects:

`ffmpeg -v debug -i 1.mp4 -vf frei0r=glow:0.5 output.mpg`

you might see something like this:

```
[Parsed_frei0r_0 @ 0x7fe7834196a0] Looking for frei0r effect in '/Users/user/.frei0r-1/lib/glow.dylib' [Parsed_frei0r_0 @ 0x7fe7834196a0] Looking for frei0r effect in '/usr/local/lib/frei0r-1/glow.dylib' [Parsed_frei0r_0 @ 0x7fe7834196a0] Looking for frei0r effect in '/usr/lib/frei0r-1/glow.dylib'
```

FFmpeg is probably right. If you will ls -l /usr/local/lib/frei0r-1/ you will see that plug-ins are installed with .so extension.

On my machine (OSX 10.12.5, ffmpeg 3.3.2, frei0r-1.6.1), I just did something like this:

`for file in /usr/local/lib/frei0r-1/*.so ; do cp $file "${file%.*}.dylib" ; done`

Also I had to set this environment variable with path to folder where these .dylib files are stored:

`export FREI0R_PATH=/usr/local/Cellar/frei0r/1.6.1/lib/frei0r-1`

This solution looks like some strange hack, but finally after all this operations I have got Frei0r working well.


FFmpeg and frei0r filter processing examples
-----------

Read about how ffmpeg works here: [https://ffmpeg.org/ffmpeg.html#Synopsis](https://ffmpeg.org/ffmpeg.html#Synopsis)

All ffmpeg filters are described here [https://ffmpeg.org/ffmpeg-filters.html#Description](https://ffmpeg.org/ffmpeg-filters.html#Description)

**1. Video and audio effects**

FFmpeg with Frei0r plugin is used for video effects.

Options `-vf` and `-af` specify simple video and audio [filters](https://ffmpeg.org/ffmpeg.html#Simple-filtergraphs)

This option `-filter_complex` specifies [complex filter](https://ffmpeg.org/ffmpeg.html#Complex-filtergraphs)

To avoid excessive audio processing we copy it unchanged from input source file with this option `-c:a copy` for all video filters.

At all only audio effects we copy video steam unchanged with this option `-c:v copy`

You could read about stream copy [here](https://ffmpeg.org/ffmpeg.html#Stream-copy)

This option is not applied for `reverse`, `slow down` and `speed up` effects because they alter both audio and video streams.

By default ffmpeg selects just one best input stream of it's kind (video, audio, subtitles, etc) let's assume that for audio effects we want all input streams, for example if they are in different languages. This is achieved with `-map 0` option.

All Frei0r plugin video filters are applied with FFmpeg simple filter option `-vf frei0r=filtername:param1_value|param2_value|param3_value`

Frei0r official page [https://frei0r.dyne.org/](https://frei0r.dyne.org/) doesn't contain any documentation on how it could be used.

During research I have found this resrouce [https://www.mltframework.org/plugins/PluginsFilters/](https://www.mltframework.org/plugins/PluginsFilters/) that describes frei0r filters along with some other differect plugins. It was very helpful for me to learn about all available filters and their parameters.

Here is original video that is used for **video effects** processing demonstration

[![No Effects](https://img.youtube.com/vi/o0Vx7Fx7aWk/0.jpg)](https://www.youtube.com/watch?v=o0Vx7Fx7aWk "No Effects")

Here is original video that was used for **audio effects** demonstration

[![No Effects](https://img.youtube.com/vi/3F-Kpc-3RYo/0.jpg)](https://www.youtube.com/watch?v=3F-Kpc-3RYo "No Effects")

**1.1 Sepia effect**

Complex filter [colorchannelmixer](https://ffmpeg.org/ffmpeg-filters.html#colorchannelmixer) adjusts video input frames by re-mixing color channels.

`-filter_complex colorchannelmixer=.393:.769:.189:0:.349:.686:.168:0:.272:.534:.131` this filter changes color temperature to be sepia like

[![Sepia Effect](https://img.youtube.com/vi/3eJzZP7KWXo/0.jpg)](https://www.youtube.com/watch?v=3eJzZP7KWXo "Sepia Effect")


**1.2 Black and White effect** 

Zero saturation with [hue](https://ffmpeg.org/ffmpeg-filters.html#hue) filter `-vf hue=s=0` produces black and white output video.

[![Black and White Effect](https://img.youtube.com/vi/wTELftdZaB0/0.jpg)](https://www.youtube.com/watch?v=wTELftdZaB0 "Black and White Effect")

**1.3 Vertigo effect**

[Vertigo](https://www.mltframework.org/plugins/FilterFrei0r-vertigo/) effect `-vf frei0r=vertigo:0.2` performas alpha blending with zoomed and rotated images. I set `phase increment` parameter to 0.2 and keep `zoomrate` parameter with default value.

[![Vertigo Effect](https://img.youtube.com/vi/RtyqzyCEyfc/0.jpg)](https://www.youtube.com/watch?v=RtyqzyCEyfc "Vertigo Effect")


**1.4 Vignette effect**

[Vignette](https://www.mltframework.org/plugins/FilterFrei0r-vignette/) effect `-vf frei0r=vignette` performs lens vignetting effect. Here it is used with default settings.

[![Vignette Effect](https://img.youtube.com/vi/QuPksoLNlTs/0.jpg)](https://www.youtube.com/watch?v=QuPksoLNlTs "Vignette Effect")

**1.5 Sobel effect**

[Sobel Effect](https://www.mltframework.org/plugins/FilterFrei0r-sobel/)
is used in image processing and computer vision, particularly within edge detection algorithms where it creates an image emphasising edges. [Read more](https://en.wikipedia.org/wiki/Sobel_operator)

It doesn't have any parameters: `-vf frei0r=sobel`

[![Sobel Effect](https://img.youtube.com/vi/ppeLcRPzlOM/0.jpg)](https://www.youtube.com/watch?v=ppeLcRPzlOM "Sobel Effect")

**1.6 Pixelizor effect**

[Pixelizor Effect](https://www.mltframework.org/plugins/FilterFrei0r-pixeliz0r/)
is applied with default settings. It creates pixelated video.

`-vf frei0r=pixeliz0r`

[![Pixelizor Effect](https://img.youtube.com/vi/61PwT7oPF-s/0.jpg)](https://www.youtube.com/watch?v=61PwT7oPF-s "Pixelizor Effect")


**1.7 Invertor effect**

[Invertor Effect](https://www.mltframework.org/plugins/FilterFrei0r-invert0r/)
inverts all colors of a source image

`-vf frei0r=invert0r`

[![Invertor Effect](https://img.youtube.com/vi/ai69LNPhB9Y/0.jpg)](https://www.youtube.com/watch?v=ai69LNPhB9Y "Invertor Effect")

**1.8 RGBnoise effect**

[RGBnoise Effect](https://www.mltframework.org/plugins/FilterFrei0r-rgbnoise/)
 it adds RGB noise to image. It has one parameter that defines amount of noise added.

`-vf frei0r=rgbnoise:0.2`

Result looks like some 90-es movie played with old video cassette player.

[![RGBnoise Effect](https://img.youtube.com/vi/TTRhpRPiKlE/0.jpg)](https://www.youtube.com/watch?v=TTRhpRPiKlE "RGBnoise Effect")

**1.9 Distorter effect**

[Distorter Effect](https://www.mltframework.org/plugins/FilterFrei0r-distort0r/)
distorts image in a weird way.

I set here two parameters: amplitude and frequency of the plasma signal.

`-vf frei0r=distort0r:0.05|0.0000001`

[![Distorter Effect](https://img.youtube.com/vi/w6Nk9oobXQE/0.jpg)](https://www.youtube.com/watch?v=w6Nk9oobXQE "Distorter Effect")

**1.10 IIRblur effect**

[IIRblur Effect](https://www.mltframework.org/plugins/FilterFrei0r-iirblur/)
provides Infinite Impulse Response Gaussian blur.

`-vf frei0r=iirblur` I use it with default settings.

[![IIRblur Effect](https://img.youtube.com/vi/qOoF6eCk6rA/0.jpg)](https://www.youtube.com/watch?v=qOoF6eCk6rA "IIRblur Effect")

**1.11 Nervous effect**

[Nervous Effect](https://www.mltframework.org/plugins/FilterFrei0r-nervous/)
it flushes frames in time in a nervous way.

Maybe this filter is not recommended for people with some neurological disorders like epilepsy.

`-vf frei0r=nervous` It doesn't have any parameters.

[![Nervous Effect](https://img.youtube.com/vi/3UZSFLNvp8s/0.jpg)](https://www.youtube.com/watch?v=3UZSFLNvp8s "Nervous Effect")

**1.12 Glow effect**

[Glow Effect](https://www.mltframework.org/plugins/FilterFrei0r-glow/) creates a Glamorous Glow.

`-vf frei0r=glow:1` `Blur of the glow` parameter is set to to maximum value.

[![Glow Effect](https://img.youtube.com/vi/BKLDN_5foUE/0.jpg)](https://www.youtube.com/watch?v=BKLDN_5foUE "Glow Effect")

**1.13 Reverse effect**

It is made with two FFmpeg filters: video reverse and audio reverse at the same time.

`-vf reverse -af areverse`

[![Reverse Effect](https://img.youtube.com/vi/fPQTDrH13GA/0.jpg)](https://www.youtube.com/watch?v=fPQTDrH13GA "Reverse Effect")

**1.14 Slow Down effect**

`-filter:v setpts=2.0*PTS -filter:a atempo=0.5`

It uses [setpts](http://ffmpeg.org/ffmpeg-all.html#setpts_002c-asetpts) filter to double [Presentation Time Stamp (PTS)](https://en.wikipedia.org/wiki/Presentation_timestamp) of original video. This results in slowing video in two times.

[atempo](http://ffmpeg.org/ffmpeg-all.html#atempo) filter is used to slow down audio in two times with 50% of original tempo setting.

[![Slow Down Effect](https://img.youtube.com/vi/f4KvxrxMKMs/0.jpg)](https://www.youtube.com/watch?v=f4KvxrxMKMs "Slow Down")

[Read more](https://trac.ffmpeg.org/wiki/How%20to%20speed%20up%20/%20slow%20down%20a%20video)

**1.15 Speed Up effect**

`-filter:v setpts=0.5*PTS -filter:a atempo=2.0` works with the same approach as slow down effect but vise versa. It takes half of original video Presentation Time Stamp and increases video tempo in two times.

[![Speed Up Effect](https://img.youtube.com/vi/SGDrpo0HIL8/0.jpg)](https://www.youtube.com/watch?v=SGDrpo0HIL8 "Speed Up Effect")

**1.16 Echo Audio effect**

[aecho](https://ffmpeg.org/ffmpeg-filters.html#aecho) audio filter is used here

`-af aecho=0.8:0.9:1000|500:0.7|0.5` first parameter is input gain of reflected signal. Second parameter is output gain of reflected signal. Next it has list of  delays from original signal in milliseconds separated by pipe `|` symbol. After this parameter goes list of decays for each delay in previous list. Colon sign `:` separates items in lists.


[![Echo Effect](https://img.youtube.com/vi/z4b7B-ncJC0/0.jpg)](https://www.youtube.com/watch?v=z4b7B-ncJC0 "Echo Effect")

**1.17 Tremolo Audio effect**

[tremolo](https://ffmpeg.org/ffmpeg-filters.html#tremolo) filter performs sinusoidal amplitude modulation.

`-af tremolo=f=10.0:d=0.7` 

`f` parameter is modulation frequency in Hertz.

`d` parameter is depth of modulation in percentage.

[![Tremolo Effect](https://img.youtube.com/vi/dnAZuxCGZs8/0.jpg)](https://www.youtube.com/watch?v=dnAZuxCGZs8 "Tremolo Effect")

**1.18 Vibrato Audio effect**

[vibrato](https://ffmpeg.org/ffmpeg-filters.html#vibrato) filter performs sinusoidal phase modulation.

`-af vibrato=f=7.0:d=0.5`

`f` parameter is modulation frequency in Hertz.

`d` parameter is depth of modulation in percentage.

[![Vibrato Effect](https://img.youtube.com/vi/MoJEZoNTpek/0.jpg)](https://www.youtube.com/watch?v=MoJEZoNTpek "Vibrato Effect")

**1.19 Chorus Audio effect**

[chorus](https://ffmpeg.org/ffmpeg-filters.html#chorus) filter resembles an echo effect with a short delay, but whereas with echo the delay is constant, with chorus, it is varied using using sinusoidal or triangular modulation.

`-af chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3`

It's parameters are ordered in such way: Input Gain, Output Gain, Delays List in ms, Decays List, Speeds List, Depths List.

Parameters are separated by pipe sign `|`, colon sign `:` separates items in lists.

[![Chorus Effect](https://img.youtube.com/vi/gT67tOFMi2w/0.jpg)](https://www.youtube.com/watch?v=gT67tOFMi2w "Chorus Effect")

**1.20. Watermark**

Watermark feature is implemented inside `carrierwave-video` gem. It uses FFmpeg's [overlay](https://ffmpeg.org/ffmpeg-filters.html#overlay-1) filter

Here is combination of such effects: *Vertigo, Vignette, Rgbnoise, Distorter, Glow, Reverse, Echo, Tremolo, Chorus, Watermark.*

Here watermark is transparent png image with red text **'FFmpeg TV'**:

[![Watermark plus many Effects](https://img.youtube.com/vi/sEnBu2VfMJo/0.jpg)](https://www.youtube.com/watch?v=sEnBu2VfMJo "Watermark plus many Effects")


Inside this article we have covered:

* FFmpeg video and audio filters
* Frei0r plugin effects

FFmpeg and Frei0r are very powerful tools. In this article I have just scratched the surface. All described filters mostly have been adding some additional distortions to video. But FFmpeg also could be used to perform video and audio enhancements, like lens correction, sound level normalization, noise and shake removal, video streaming and much more.

