Suggested Metatags: ffmpeg, frei0r, video effects, sound effects, video processing, sidekiq, carrierwave_backgrounder, carrierwave, streamio-ffmpeg, ruby, rails, mongoid, mongodb, nosql, action cable, progress bar

**Article #2: Advanced Video Processing at Ruby on Rails with FFmpeg filters and Frei0r plugin effects**
=============

Introduction
-------------

This is second article about video processing with Rails. Here I am going to describe more advanced things like video and audio filters, reading video metadata, making screenshots for each second of video, and real time monitoring of video processing progress with Action Cable. All these features are going to added to application that was developed in first article. **TODO: Add link to first article**


Results of video processing experiments could be found in third article **TODO: Add link to third article**

TL;DR

If you do not want to read all this article than project repository is here:
[https://github.com/Yalantis/video-manipulator](https://github.com/Yalantis/video-manipulator)

Project Setup
-------------

**1. Install FFmpeg and all needed software**

You will need need: ffmpeg with Frei0r plugin, mongoDB and redis, ruby, rails and rvm.

**FFmpeg Installation:**

If your operating system is OSX than you could use homebrew for installation

```
brew install ffmpeg --with-fdk-aac --with-frei0r --with-libvo-aacenc --with-libvorbis --with-libvpx --with-opencore-amr --with-openjpeg --with-opus --with-schroedinger --with-theora --with-tools
```

Maybe not all of these options are needed for current project but I included almost all possible to avoid issues when something is not working if some extension has not been installed.

**2. Fix frei0r**

When I installed ffmpeg with Frei0r, it's effects were not working. To check if you have such issue run ffmpeg with frei0r effects:

`ffmpeg -v debug -i 1.mp4 -vf frei0r=glow:0.5 output.mpg`

you might see something like this:

```
[Parsed_frei0r_0 @ 0x7fe7834196a0] Looking for frei0r effect in '/Users/user/.frei0r-1/lib/glow.dylib' [Parsed_frei0r_0 @ 0x7fe7834196a0] Looking for frei0r effect in '/usr/local/lib/frei0r-1/glow.dylib' [Parsed_frei0r_0 @ 0x7fe7834196a0] Looking for frei0r effect in '/usr/lib/frei0r-1/glow.dylib'
```

FFmpeg is probably right. If you will ls -l /usr/local/lib/frei0r-1/ you will see that plug-ins are installed with .so extension.

On my machine (OSX 10.12.5, ffmpeg 3.3.2, frei0r-1.6.1), I just did something like this:

`for file in /usr/local/lib/frei0r-1/*.so ; do cp $file "${file%.*}.dylib" ; done`

Also I had to set this environment variable with path to folder where these .dylib files are stored:

`export FREI0R_PATH=/usr/local/Cellar/frei0r/1.6.1/lib/frei0r-1`

This solution looks like some strange hack, but finally after all this operations I have got Frei0r working well.


Development
-----------

**1. Video And Audio Filters**

Let's define all effects that would used with this app inside `EncodingConstants` module:

```
module EncodingConstants
  #  ...

  VIDEO_EFFECTS = {
    sepia:
      %w[
        -filter_complex colorchannelmixer=.393:.769:.189:0:.349:.686:.168:0:.272:.534:.131
        -c:a
        copy
      ],
    black_and_white: %w[-vf hue=s=0 -c:a copy],
    vertigo: %w[-vf frei0r=vertigo:0.2 -c:a copy],
    vignette: %w[-vf frei0r=vignette -c:a copy],
    sobel: %w[-vf frei0r=sobel -c:a copy],
    pixelizor: %w[-vf frei0r=pixeliz0r -c:a copy],
    invertor: %w[-vf frei0r=invert0r -c:a copy],
    rgbnoise: %w[-vf frei0r=rgbnoise:0.2 -c:a copy],
    distorter: %w[-vf frei0r=distort0r:0.05|0.0000001 -c:a copy],
    iirblur: %w[-vf frei0r=iirblur -c:a copy],
    nervous: %w[-vf frei0r=nervous -c:a copy],
    glow: %w[-vf frei0r=glow:1 -c:a copy],
    reverse: %w[-vf reverse -af areverse],
    slow_down: %w[-filter:v setpts=2.0*PTS -filter:a atempo=0.5],
    speed_up: %w[-filter:v setpts=0.5*PTS -filter:a atempo=2.0]
  }.freeze

  AUDIO_EFFECTS = {
    echo: %w[-map 0 -c:v copy -af aecho=0.8:0.9:1000|500:0.7|0.5],
    tremolo: %w[-map 0 -c:v copy -af tremolo=f=10.0:d=0.7],
    vibrato: %w[-map 0 -c:v copy -af vibrato=f=7.0:d=0.5],
    chorus: %w[-map 0 -c:v copy -af chorus=0.5:0.9:50|60|40:0.4|0.32|0.3:0.25|0.4|0.3:2|2.3|1.3]
  }.freeze

  EFFECT_PARAMS = VIDEO_EFFECTS.merge(AUDIO_EFFECTS).freeze

  ALLOWED_EFFECTS = EFFECT_PARAMS.keys.map(&:to_s).freeze

  # ...
end
```

**Suggested Improvement: All these filters are defined as constants here for simplicity since it is demo application. But in production I would prefer to create hierarchy of classes for these filters. This would let us set all filter parameters from frontend side and validate that at backend.**

We allow user to specify multiple effects for video processing. Than let's add array attribute to `Video` model that would contain all user selected effects:

```
  # All user applied effects is stored as array
  field :effects, type: Array, default: []
```

Also we have to validate that user selected only available effects:

```
  validate :effects_allowed_check
  
  private

  def effects_allowed_check
    effects.each do |effect|
      unless ::VideoUploader::ALLOWED_EFFECTS.include?(effect)
        errors.add(:effects, :not_allowed, effect: effect.humanize)
      end
    end
  end
```

At create/update video form we have this as set of checkboxes (`materialize-sass` has been used for styling):


```
  <div class="row">
    <span>Effects</span>
    <% ::VideoUploader::ALLOWED_EFFECTS.each do |effect| %>
      <div class="input-field">
        <%= f.check_box :effects, {id: "effects_#{effect}", multiple: true, checked: f.object.effects.include?(effect) }, effect, nil %>
        <%= f.label :effects, effect.humanize, id: "effects_#{effect}", for: "effects_#{effect}" %>
      </div>
    <% end %>
  </div>
```

Next at `VideoUploader` we have to include our `EncodingConstants` module and add encoding step for effects processing after normalization step. Logic of `normalization_step` was defined in first article it just converts input video to mp4 format.

```
  # ...
  include ::EncodingConstants
  
  process encode: [:mp4, PROCESSED_DEFAULTS]

  def encode(format, opts = {})
    normalization_step(format, opts)
    apply_effect_steps(format, opts)
  end
  
  private
    
  def audio_effects
    model.effects & AUDIO_EFFECTS.keys.map(&:to_s)
  end

  def video_effects
    model.effects & VIDEO_EFFECTS.keys.map(&:to_s)
  end

  def ordered_effects
    audio_effects + video_effects
  end

  def apply_effect_steps(format, opts)
    ordered_effects.each do |effect|
      encode_video(
        format,
        opts.merge(
          processing_metadata: { step: "apply_#{effect}_effect" }
        )
      ) do |_, params|
        params[:custom] = EFFECT_PARAMS[effect.to_sym]
      end
    end
  end
  
  # ...
  
```

Full video uploader code is presented here [https://github.com/Yalantis/video-manipulator/blob/master/app/uploaders/video_uploader.rb](https://github.com/Yalantis/video-manipulator/blob/master/app/uploaders/video_uploader.rb)

Processing of all user selected effects happens one by one. Some effects could be applied at one transcoding operation some are not compatible with each other, so due this we process them separately to be sure that everything could be processed properly. Of course it is slower than transcoding all in one operation, but it is more reliable. Also audio effects are applied before video effects. I had some incompatibility errors when audio effects were applied after video effects, so I put them at first place in this effects processing order at `ordered_effects` method. Option `:custom` from `carrierwave-video` gem is used to provide filtering parameters to ffmpeg.

**Suggested improvement: It is better to have ability to chain these effects in similar way to Rails Active Record scopes. This would let user define effects order and quantity, especially if these effects would have ability to accept user provided filtering parameters**

**2. Video Watermark**

This feature was described in first article. In this app it is moved to separate processing step at `VideoUploader`:

```
  def encode(format, opts = {})
    normalization_step(format, opts)
    apply_effect_steps(format, opts)
    apply_watermark_step(format, opts) if model.watermark_image.path.present?
    # ...
  end


  private
  
   def apply_watermark_step(format, opts)
    encode_video(
      format,
      opts.merge(processing_metadata: { step: 'apply_watermark' })
    ) do |_, params|
      params[:watermark] ||= {}
      params[:watermark][:path] = model.watermark_image.path
    end
  end

```

**3. Reading Video Metadata**

Let's add hash attribute to `Video` model it would store video metadata. And also it would require processing callback method that would save metadata there. `file_duration` is updated there too.

```
  # File ffmpeg metadata is stored at hash
  field :metadata, type: Hash, default: {}
  
  # Callback method
  def save_metadata(new_metadata)
    set(
      metadata: new_metadata,
      file_duration: new_metadata[:format][:duration]
    )
  end
```

Next we need `::CarrierWave::Extensions::VideoMetadata` module with metadata processing logic:

It has just one parameter `save_metadata_method` that contains the name of callback at model that is called with extracted metadata. It's code is made to be compatible with `carrierwave-video` gem that's why it has similar logic with it's [encode_video](https://github.com/evgeniy-trebin/carrierwave-video/blob/master/lib/carrierwave/video.rb#L41-L71) method.

Also there is code to provide progress of this operation value. But since it's instant then it sends 1.0 (as 100%) to progress callback method just once.

```
module CarrierWave
  module Extensions
    module VideoMetadata
      def read_video_metadata(format, opts = {})
        # move upload to local cache
        cache_stored_file! unless cached?

        @options = CarrierWave::Video::FfmpegOptions.new(format, opts)

        file = ::FFMPEG::Movie.new(current_path)

        if opts[:save_metadata_method]
          model.send(opts[:save_metadata_method], file.metadata)
        end

        progress = @options.progress(model)

        with_trancoding_callbacks do
          # Here it happens instantly so we provide here value for 100%
          progress&.call(1.0)
        end
      end
    end
  end
end
```

Than at `VideoUploader` we use this logic at `encode` method after watermarking step:

```
  def encode(format, opts = {})
    # ...
    apply_watermark_step(format, opts) if model.watermark_image.path.present?
    read_video_metadata_step(format, opts)
    # ...
  end
  
  private
  
  def read_video_metadata_step(format, opts)
    read_video_metadata(
      format,
      opts.merge(
        save_metadata_method: :save_metadata,
        processing_metadata: { step: 'read_video_metadata' }
      )
    )
  end
```

**4. Thumbnails Generation**

Add `Thumbnail` model. It is embedded inside `Video`. Each `Thumbnail` record would store just one thumbnail image:

```
class Thumbnail
  include Mongoid::Document

  embedded_in :video

  # Here background uploading is not used since this entity would be created in
  # background already
  mount_uploader :file, ::ImageUploader
end
```

Next we turn back to `Video` model again. It needs definition of `thumbnails` association and callback method for thumbnails saving:

```
  embeds_many :thumbnails
  
  # Config option: generate thumbnails for each video second or not
  field :needs_thumbnails, type: Boolean, default: false
  
  # Callback method
  def save_thumbnail_files(files_list)
    files_list.each do |file_path|
      ::File.open(file_path, 'r') do |f|
        thumbnails.create!(file: f)
      end
    end
  end
```

Next let's place all thumbnails creation logic to `CarrierWave::Extensions::VideoMultiThumbnailer` module. It is also designed to be processed with `carrierwave-video` gem's logic.

This file is too big to be fully presented inside artice. For full file look inside repository [https://github.com/Yalantis/video-manipulator/blob/master/app/classes/carrier_wave/extensions/video_multi_thumbnailer.rb](https://github.com/Yalantis/video-manipulator/blob/master/app/classes/carrier_wave/extensions/video_multi_thumbnailer.rb)

Here is main part of this file:

```
module CarrierWave
  module Extensions
    module VideoMultiThumbnailer
      def create_thumbnails_for_video(format, opts = {})
        prepare_thumbnailing_parameters_by(format, opts)
        # Create temporary directory where all created thumbnails would be saved
        prepare_tmp_dir

        run_thumbnails_transcoding

        # Run callback for saving thumbnails
        save_thumb_files
        # Remove temporary data
        remove_tmp_dir
      end

      private

      def run_thumbnails_transcoding
        with_trancoding_callbacks do
          if @progress
            @movie.screenshot(*screenshot_options) do |value|
              @progress.call(value)
            end
            # It is ugly hack but this operation returned this in the end
            # 0.8597883597883599 that is not 1.0
            @progress.call(1.0)
          else
            @movie.screenshot(*screenshot_options)
          end
        end
      end

	   # Some method definitions are skipped here. Look into repostory for more details.

      # Thumbnails are sorted by their creation date
      # to align then in chronological order.
      def thumb_file_paths_list
        Dir["#{tmp_dir_path}/*.#{@options.format}"].sort_by do |filename|
          File.mtime(filename)
        end
      end

      def save_thumb_files
        model.send(@options.raw[:save_thumbnail_files_method], thumb_file_paths_list)
      end

      def screenshot_options
        [
          "#{tmp_dir_path}/%d.#{@options.format}",
          @options.format_params,
          {
            preserve_aspect_ratio: :width,
            validate: false
          }
        ]
      end
    end
  end
end
```

It uses `screenshot` method directly from [streamio-ffmpeg](https://github.com/streamio/streamio-ffmpeg#taking-screenshots) gem. In our case it generates thumbnails for each second of video. Also since this operation is local we have to create, use and remove temporary directories for generated thumbnails with `tmp_dir_path`. Generated images are sent to model's callback that is specified at `save_thumbnail_files_method` parameter.

By some strange reason it's progress value does not always return `1.0` at the end (`0.8597` for example) that is why I had to set manually `1.0` progress value when operation has been finished.

Inside `VideoUploader` we update include this module and update `encode` method with `create_thumbnails_step`:

```
  include ::CarrierWave::Extensions::VideoMultiThumbnailer

  def encode(format, opts = {})
    # ...
    read_video_metadata_step(format, opts)
    create_thumbnails_step('jpg', opts) if model.needs_thumbnails?
  end
  
  private
  
  def create_thumbnails_step(format, _opts)
    create_thumbnails_for_video(
      format,
      progress: :processing_progress,
      save_thumbnail_files_method: :save_thumbnail_files,
      resolution: '300x300',
      vframes: model.file_duration, frame_rate: '1', # create thumb for each second of the video
      processing_metadata: { step: 'create_video_thumbnails' }
    )
  end
```

**5. Progress Calculation logic**

Since each transcoding operation could return progress values, I decided to play with it also. Our video processing consists of many steps, each step has it's own progress value, so we need to calculate overall progress that consists of all these steps.

When I was researching this I noticed that progress callback is called very often, near once or twice per second. Since I want to track this progress in database, I decided to update it once per 10% of each step's transcoding progress.

At `EncodingConstants` inside `PROCESSED_DEFAULTS` we have definition of `progress` callback method ([Read more here](https://github.com/evgeniy-trebin/carrierwave-video#possible-options)):

```
  PROCESSED_DEFAULTS = {
    # ...
    progress: :processing_progress
  }.freeze
```

Also you might notice that inside each processing step we have this:

```
  opts.merge(processing_metadata: { step: '<step-name-here>' })
```

It is passed inside `format_options` to `processing_progress` callback method at `Video` model:

```
  def processing_progress(format, format_options, new_progress)
    ProgressCalculator.new(self, format, format_options, new_progress).update!
  end
```

It uses `ProgressCalculator` instance to handle all this progress percentage calculation, database updates and websocket notifications:

```
class ProgressCalculator
  attr_reader :video, :format, :format_options, :new_progress

  def initialize(video, format, format_options, new_progress)
    @video = video
    @format = format
    @format_options = format_options
    @new_progress = new_progress
  end

  def update!
    update_progress_data if shoud_update?
  end

  private

  def update_progress_data
    # Had to use atomic set operation here since normal update
    # has been setting file_processing to false while processing still was not finished
    step_metadata.set(progress: new_progress.to_f)
    video.set(progress: calculate_overall_progress.to_f)
    notify_about_progress
  end

  def step
    format_options[:processing_metadata][:step]
  end

  def step_metadata
    video.processing_metadatas.find_or_create_by!(step: step) do |pm|
      pm.format = format
    end
  end

  def diff
    new_progress.to_f - step_metadata.progress.to_f
  end

  def shoud_update?
    # Update this value only each 10th percent or when processing is finished
    diff >= 0.1 || (new_progress.to_i == 1)
  end

  def steps_count
    # Normalize step + 1
    # Effects steps + effects.count
    # Watermark step + 1
    # Read video metadata step + 1
    # Generate thumbnails step + 1
    count = ::VideoUploader::OBLIGATORY_STEPS.count + video.effects.count
    count += 1 if video.watermark_image.path.present?
    count += 1 if video.needs_thumbnails?
    count
  end

  def calculate_overall_progress
    video.processing_metadatas.sum(:progress) / steps_count
  end

  def notify_about_progress
    ::ActionCable.server.broadcast(
      'notifications_channel',
      progress_payload
    )
  end

  def progress_payload
    ::CableData::VideoProcessing::ProgressSerializer.new(video).as_json
  end
end
```

Method `notify_about_progress` uses rails `ActionCable` for progress update on video details page. It would be described little bit later.

**6. Carrierwave Video Callbacks**

`carrierwave-video` gem has nice feature as `before_transcode` and `after_transcode` callbacks [https://github.com/evgeniy-trebin/carrierwave-video#possible-options](https://github.com/evgeniy-trebin/carrierwave-video#possible-options)

I use it to clean previously generated thumbnails and progress metadatas if video file has been updated.

At `Video` model here is callback

```
  # before_transcode callback
  # Callback method accepts format and raw options but we do not need them here
  def processing_init_callback(_, _)
    # Clean datas
  end
```

That is called at `normalization_step` inside `VideoUploader` before new transcoding:

```
  def normalization_step(format, opts)
    encode_video(
      format,
      opts.merge(
        processing_metadata: { step: 'normalize' },
        callbacks: {
          # Clean previous progress data if encoding happens for existing video record
          # Callback method at model
          before_transcode: :processing_init_callback
        }
      )
    )
  end
```

**7. Custom video saver worker**

We are using background workers for video processing and video storage (in production scenario this worker uploads file to cloud like Amazon S3). Event when video processing is ready happens there. So, we have to update page with processed video from this background worker.

Also we use sidekiq workers directly without rails' Active Job, since it has been failing at this storage worker. It is related to compatibility issue with new rails and this `carrierwave_backgrounder` gem.

So we redefine it's `perform` method to run custom callbacks here `run_video_processing_chain_completed_callbacks`.

```
class VideoSaverWorker < ::CarrierWave::Workers::StoreAsset
  def perform(*args)
    super(*args)
    run_video_processing_chain_completed_callbacks
  end

  private

  def run_video_processing_chain_completed_callbacks
    record.processing_completed_callback if record.respond_to?(:processing_completed_callback)
  end
end
```

`processing_completed_callback` is defined at `Video` model. It sends Action Cable websocket notification with sign that processing has been finished.

```
  def processing_completed_callback
    ::ActionCable.server.broadcast(
      'notifications_channel',
      ::CableData::VideoProcessing::CompletedSerializer.new(self).as_json
    )
  end
```

**8. ActionCable and WebSocket notifications**

For instant processing progress updates via Web Sockets we use `ActionCable`.

Use this command to generate Notifications channel `rails generate channel Notifications`.

Since we want to use redis for ActionCable websocket notifications in development environment too we have to change this config: `config / cable.yml`:

```
development:
  adapter: redis
  url: redis://localhost:6379/1
  ...
```

As we do not have any users and authorizations all browsers which have opened our site are subscribed to `notifications_channel`.

`app/assets/javascripts/channels/notifications.coffee`:

```
App.notifications = App.cable.subscriptions.create "NotificationsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server
    console.log('Connected')

  disconnected: ->
    # Called when the subscription has been terminated by the server
    console.log('Disconnected')

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    if data['processing_completed'] == false
      $("#video_progress").replaceWith($(data['html']))
    else
      $("#video_info").replaceWith($(data['html']))
      if $('#thumbnails').length
        # Init thumbnails carousel when they are present only
        $('.carousel').carousel({})
```

After each webscoket notification it updates video processing progress information. And when processing is finished it replaces all page content with data that have been delivered by Action Cable.

This code it not the best way to this. Normally you should send JSON data over websockets and all styling and formatting should happen at frontend side. But let's skip this. This is made to just describe idea.

Channel is defined here at backend `app/channels/notifications_channel.rb`:

```
class NotificationsChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'notifications_channel'
  end
  ...
end

```

In code we have two places where we send websocket notifications from server to `notifications_channel`:

* `ProgressCalculator#notify_about_progress` during each progress update
* `Video#processing_completed_callback` when processing has been finished and file is ready.

Processing Progress Action Cable Notification uses `CableData::VideoProcessing:: ProgressSerializer`  to generate notification payload:

```
module CableData
  module VideoProcessing
    class ProgressSerializer < ActiveModel::Serializer
      attributes :html, :processing_completed

      def html
        # INFO: This is made for simplicity
        #       But in real application it is better to send
        #       JSON data via action cable only and process
        #       all styling and markup at frontend side
        ApplicationController.renderer.render(
          locals: { video: object },
          partial: 'videos/progress'
        )
      end

      def processing_completed
        false
      end
    end
  end
end
```

Processing Completed Action Cable Notification uses `CableData::VideoProcessing::CompletedSerializer` to generate notification payload:

```
module CableData
  module VideoProcessing
    class CompletedSerializer < ActiveModel::Serializer
      attributes :html, :processing_completed

      def html
        # INFO: This is made for simplicity
        #       But in real application it is better to send
        #       JSON data via action cable only and process
        #       all styling and markup at frontend side
        ApplicationController.renderer.render(
          locals: { video: object },
          partial: 'videos/info'
        )
      end

      def processing_completed
        true
      end
    end
  end
end
```

Conclusions
-----------

Inside this article we have covered:

* FFmpeg video and audio filters and Frei0r plugin effects.
* Processing these filters with `carrierwave-video` gem
* Reading and saving Video Metadata with `streamio-ffmpeg` and `carrierwave-video` gems
* Generation thumbnails for each second of video with `streamio-ffmpeg` gem's `FFMPEG::Movie#screenshot` method.
* Video processing progress calculation per each processing step with `carrierwave-video` gem's `progress` callback method.
* Real time progress bar update through websockets with rails's Action Cable.
* Before transcoding callback `before_transcode` from `carrierwave-video` gem
* Added custom `VideoSaverWorker` as video saver worker instead of default `carrierwave_backgrounder` gem's one. To let us send websocket notification at moment when video is processing and saving has been finished.


Since it is demo application it could be improved to be production ready with these things:

* Errors handling. Maybe something user friendly. Since now it just silently retries processing inside background worker.
* All filters are defined as constants for simplicity since it is demo application. But in production it would be better to create hierarchy of classes for these filters or use factory pattern. This would let us set all filter parameters from frontend side and validate that at backend. Making it more customizable for users.
* It is better to have ability to chain these effects in similar way to Rails Active Record scopes. This would let user define effects order and quantity, especially if these effects would have ability to accept user provided filtering parameters